/*

File : manche.scad

Author : Pauline Mackelbert

Date : 14 juin 2022

License : Creative Attribution-NonCommercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

*/

$fn=200;

hauteur_manche = 100;
largeur_manche = 28.1;
profondeur_manche = 10;

cube([largeur_manche,profondeur_manche,hauteur_manche],center=true);

nombre_lame = 7;
epaisseur_lame = 0.3;
nombre_espace = nombre_lame + 1;
epaisseur_espace = 2;
dimension_interne = 5;
largeur_accroche = nombre_lame*epaisseur_lame+nombre_espace*epaisseur_espace+2*dimension_interne;


translate([0,0,(hauteur_manche+dimension_interne)/2])cube([largeur_accroche,profondeur_manche,dimension_interne],center=true);

diameter_lame= 45;
rayon_axe = 2;
hauteur_accroche = diameter_lame/2 + rayon_axe + 15;

translate([(largeur_accroche-dimension_interne)/2,0,hauteur_manche/2+dimension_interne+hauteur_accroche/2])cube([dimension_interne,profondeur_manche,hauteur_accroche],center=true);
translate([-(largeur_accroche-dimension_interne)/2,0,hauteur_manche/2+dimension_interne+hauteur_accroche/2])cube([dimension_interne,profondeur_manche,hauteur_accroche],center=true);

hauteur_axe_accroche = diameter_lame/2 + 10;
largeur_axe = 5;

translate([largeur_accroche/2 - dimension_interne - largeur_axe/2,0,hauteur_manche/2 + dimension_interne +hauteur_axe_accroche])rotate([0,90,0])cylinder(h = largeur_axe, r = rayon_axe, center=true);
translate([-largeur_accroche/2 + dimension_interne + largeur_axe/2,0,hauteur_manche/2 + dimension_interne +hauteur_axe_accroche])rotate([0,90,0])cylinder(h = largeur_axe, r = rayon_axe, center=true);

