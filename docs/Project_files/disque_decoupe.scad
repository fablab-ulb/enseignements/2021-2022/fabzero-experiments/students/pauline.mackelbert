/*

File : disque_decoupe.scad

Author : Pauline Mackelbert

Date : 14 juin 2022

License : Creative Attribution-NonCommercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

*/

include <BOSL/constants.scad>
use <BOSL/shapes.scad>
$fn=200;

longueur_coupe = 60.7;
longueur_espace = 10;
nombre_de_multiple = 5;
perimetre = nombre_de_multiple*longueur_coupe + nombre_de_multiple*longueur_espace;
rayon_disque_decoupe = perimetre / (2*PI);
hauteur_disque_decoupe = 0.5;
angle_grand_secteur = (longueur_espace*360)/(2*PI*rayon_disque_decoupe);

rayon_disque_centre = rayon_disque_decoupe - 10;
hauteur_disque_centre = 2;
rayon_petit_secteur = (rayon_disque_decoupe*sin(angle_grand_secteur/2)) / cos(atan((rayon_disque_decoupe*cos(angle_grand_secteur/2) - rayon_disque_centre) / (rayon_disque_decoupe*sin(angle_grand_secteur/2))));
angle_petit_secteur = 90 - atan((rayon_disque_decoupe * cos(angle_grand_secteur / 2))-rayon_disque_centre)/(rayon_disque_decoupe*sin(angle_grand_secteur/2));

module disque_decoupe(hauteur_disque_decoupe,rayon_disque_decoupe){
cylinder(h = hauteur_disque_decoupe, r = rayon_disque_decoupe, center=true);
}

module disque_centre(hauteur_disque_centre,rayon_disque_centre){
cylinder(h = hauteur_disque_centre, r = rayon_disque_centre, center=true);
}

module grand_secteur(hauteur_disque_decoupe,rayon_disque_decoupe,angle_grand_secteur) {
translate([0,0,-hauteur_disque_decoupe/2])pie_slice(ang=angle_grand_secteur,h=hauteur_disque_decoupe,r=rayon_disque_decoupe);
}

*difference(){
    disque_decoupe(hauteur_disque_decoupe,rayon_disque_decoupe);
    grand_secteur(hauteur_disque_decoupe,rayon_disque_decoupe,angle_grand_secteur);
    for (i=[1:nombre_de_multiple-1]){
    rotate([0,0,i*(360/nombre_de_multiple)])grand_secteur(hauteur_disque_decoupe,rayon_disque_decoupe,angle_grand_secteur);
    }
}

module petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre){
points = [[0,0],[rayon_disque_decoupe * cos(angle_grand_secteur / 2)-rayon_disque_centre+1,0],[rayon_disque_decoupe * cos(angle_grand_secteur / 2)-rayon_disque_centre,rayon_disque_decoupe*sin(angle_grand_secteur/2)]];
translate([rayon_disque_centre,0,0])polygon(points);  
}

module double_petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre){
    
    petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre);
    mirror([0,1,0])petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre);
}

difference(){
    disque_decoupe(hauteur_disque_decoupe,rayon_disque_decoupe);
    %grand_secteur(hauteur_disque_decoupe,rayon_disque_decoupe,angle_grand_secteur);
    double_petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre);
    for (i=[1:nombre_de_multiple-1]){
    rotate([0,0,i*(360/nombre_de_multiple)])double_petit_secteur(rayon_disque_decoupe,angle_grand_secteur,rayon_disque_centre);
    }
}
translate([0,0,1])disque_centre(hauteur_disque_centre,rayon_disque_centre);