/*

File : space.scad

Author : Pauline Mackelbert

Date : 14 juin 2022

License : Creative Attribution-NonCommercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

*/

include <BOSL/constants.scad>
use <BOSL/shapes.scad>
$fn=200;

rayon_espace = 35/2;
hauteur_espace = 2;

rayon_interne_petit = 4.1;
rayon_interne_grand = 5.1;
angle_section = 360/6;
rayon_axe = 2.1;

nombre_lame = 7;
epaisseur_lame = 0.3;
nombre_espace = nombre_lame + 1;
epaisseur_espace = 2;
hauteur = nombre_lame*epaisseur_lame+nombre_espace*epaisseur_espace;

module secteur(hauteur,angle_section,rayon_interne_grand) {
translate([0,0,-hauteur/2])pie_slice(ang=angle_section,h=hauteur,r=rayon_interne_grand);
}

module axe(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit){
difference(){
    cylinder(h = hauteur, r = rayon_interne_grand, center=true);
    secteur(hauteur,angle_section,rayon_interne_grand);
    rotate([0,0,120])secteur(hauteur,angle_section,rayon_interne_grand);
    rotate([0,0,240])secteur(hauteur,angle_section,rayon_interne_grand);
}
    cylinder(h = hauteur, r = rayon_interne_petit, center=true);
}
module espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace){
difference(){
cylinder(h = hauteur_espace, r = rayon_espace, center=true);
axe(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit);
}
}

espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([rayon_espace*2+3,0,0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([-(rayon_espace*2+3),0,0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([rayon_espace*2+3,rayon_espace*2+3,0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([-(rayon_espace*2+3),-(rayon_espace*2+3),0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([rayon_espace*2+3,-(rayon_espace*2+3),0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);
translate([-(rayon_espace*2+3),rayon_espace*2+3,0])espace(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit, hauteur_espace, rayon_espace);