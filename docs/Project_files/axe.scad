/*

File : axe.scad

Author : Pauline Mackelbert

Date : 14 juin 2022

License : Creative Attribution-NonCommercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

*/

include <BOSL/constants.scad>
use <BOSL/shapes.scad>
$fn=200;

rayon_interne_petit = 4;
rayon_interne_grand = 5;
angle_section = 360/6;
rayon_axe = 2.2;

nombre_lame = 7;
epaisseur_lame = 0.3;
nombre_espace = nombre_lame + 1;
epaisseur_espace = 2;
hauteur = nombre_lame*epaisseur_lame+nombre_espace*epaisseur_espace;

module secteur(hauteur,angle_section,rayon_interne_grand) {
translate([0,0,-hauteur/2])pie_slice(ang=angle_section,h=hauteur,r=rayon_interne_grand);
}

module axe(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit){
difference(){
    cylinder(h = hauteur, r = rayon_interne_grand, center=true);
    secteur(hauteur,angle_section,rayon_interne_grand);
    rotate([0,0,120])secteur(hauteur,angle_section,rayon_interne_grand);
    rotate([0,0,240])secteur(hauteur,angle_section,rayon_interne_grand);
}
    cylinder(h = hauteur, r = rayon_interne_petit, center=true);
}

difference(){
axe(hauteur,rayon_interne_grand,angle_section,rayon_interne_petit);
cylinder(h=hauteur,r=rayon_axe,center=true);
}