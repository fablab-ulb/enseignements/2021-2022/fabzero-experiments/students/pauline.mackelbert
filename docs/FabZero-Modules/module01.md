# 1. Les fondamentaux

Lors de cette première séance, nous avons fait connaissance, nous avons découvert ce qu'est la science frugale ainsi que la fabrication digitale. Nous avons ensuite visité le FabLab ULB dont le Frugal LAB. Finalement nous avons appris les bases d'une bonne documentation.

## 1.1 La fabrication numérique

Ce cours est inspiré de celui créé par Neil Gershenfeld "How to Make Almost Anything"[^1]. Il s'était rendu compte que cela prenait trop de temps d'enseigner comment utiliser toutes les machines numériques. Il a alors décidé d'enseigner comment utiliser les machines-outils à commandes numériques de base et à partir de cela ses étudiants seraient capable de "faire à peu près n'importe quoi" sur quelconque machine.

[^1]:[Neil Gershenfeld - How to Make Almost Anything](https://www.youtube.com/watch?v=EYqt2d0fOr8)

Les FabLab s'en inspirent en enseignant et en mettant à disposition certaines de ces machines.

## 1.2 La documentation

La documentation permet de garder une trace de nos apprentissages et des difficultés qu'on a pu rencontrer. C'est très utile car sans cela il serait difficile de se souvenir de tout. C'est également intéressant car cela permet de partager notre documentation avec d'autres personnes. Nous utilisons par conséquent un *version control system*.

*Version control system*[^2]
  : "*Version control systems are a category of software tools that helps in recording changes made to files by keeping a track of modification done to the code*"

[^2]: [Version control systems](https://www.geeksforgeeks.org/version-control-systems/)

Pour écrire cette documentation plusieurs outils sont utilisés :

- CLI
- GitLab
- Git
- Docs

Avant d'écrire ma description ainsi que mes premières idées de projet sur mon site web, je vais d'abord comprendre ces outils en m'aidant de la documentation disponible sur [FabZero](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#git).

### 1.2.1 *Command Line user Interface* (CLI)

Dans les différents programmes que nous pouvons utiliser on a l'habitude de travailler avec des GUI.

*Graphical User Interface* (GUI)[^3]
 : "Il s'agit de la manière selon laquelle un logiciel est présenté à un utilisateur sur un écran. Pour faire simple, l'interface graphique, ou GUI, se résume à l'affichage des commandes permettant d'effectuer des actions dans un logiciel, comme des menus, des boutons, des fonctionnalités, etc., sans avoir à saisir des lignes de commandes."

[^3]: [GUI](https://www.journaldunet.fr/web-tech/dictionnaire-du-webmastering/1203287-gui-graphical-user-interface-definition-traduction/#:~:text=La%20GUI%2C%20pour%20Graphical%20User,un%20utilisateur%20sur%20un%20%C3%A9cran.)

Pourtant travailler avec un *Command Line user Interface* / terminal en utilisant des commandes UNIX est très puissant. En effet, cela est plus rapide pour exécuter les programmes. De plus, certains programmes ne fournissent pas de GUI.

L'environnement que nous allons utiliser est BASH, il s'agit d'un *Shell* / *command-line interpreter*. Les informations qui suivent concernant BASH sont inspirées des sources suivantes : [FabAcademy](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md) & [Basic UNIX commands](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html).

Une *command line* est une combinaison de plusieurs élements : *command*, *option* et *argument*.

Voici une liste des commandes de bases :

| *Command* |Description | *Option* | Description
| :--- | :--- | :--- | :---
|`ls` | *List files and folders*|`ls -a` ou `ls --all` | *Display hidden files*
| | | `ls -l` | *Long list format*
| | | `ls -h` | *Human readable*
| `pwd` | *Print working directory* | |
| `cd` | *Change directory* | `cd -` | *Go to the last directory*
| `mkdir` | *Create a directory* | `mkdir -p` parent/child | Parent directories
|`touch file.txt` | *Create an empty file* | |
| `rmdir` | *Remove empty directories* | |
| `cp file.txt copy.txt` | *Copy files and folders* | |
| `mv file.txt file2.txt` | *Move files or rename files* | `mv -r` | *Recursive*
|`find` | *Recursively search for files and folders inside a specified starting location*
| `cat` | *Display file on the screen* | `cat -n` | *Number the lines*
| | | `cat file.txt | clip` | *Does not display but copy the content*
|`wc` | *Count characters, words, lines*
| `head`| *Show the first 10 lines of a file* | `head -n` | *Change number of lines*
| `tail` | *Show latest 10 lines of a file* | `tail -n 5 file.txt` | *Change number of lines (here 5)*
| `grep "pattern"` | *Look for text inside files and display the line of text that contains the search pattern* | `grep -r` | *Recursive search*
| | | `grep -i` | *Case insensitive search pattern*
| | | `grep -n` | *Show the text line number that contains the search pattern*
| `echo` | *Display its parameters in the stdout (screen)* | `echo text > file.txt` | *Write test to file (overwrite!)*
| | | `echo text >> file.txt` | *Append text to file (new line)*
|`sudo command` | *Super User Do, to get permission* | `sudo -k` | *No more admin privileges*

Il faut faire attention à quelques points :

- *case sensitive* (il y a une différence entre les minuscules et les majuscules)
- pour écrire "deux mots" il faut écrire "deux\ mots" pour respécter les espaces
- pour avoir des informations sur une commande il faut écrire `command --help`
- plusieurs options peuvent être combinées, il suffit de les concaténer (ex: `-ri`)

### 1.2.2 GitLab

Pour mettre à jour notre site web, nous allons utiliser le GUI Gitlab.com. Pour le faire il faut :

1. se connecter à GitLab
2. aller sur [Pauline Mackelbert](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert)
3. modifier les fichiers markdown (.md)

Comme il était conseillé, j'ai également activé les notifications de GitLab par email en suivant les [instructions](https://docs.gitlab.com/ee/user/profile/notifications.html#editing-notification-settings).

### 1.2.3 Git

Pour éviter de devoir toujours se connecter sur le serveur de GitLab, il est également possible de travailler à distance. Grâce à Git je vais faire une copie du projet sur mon ordinateur. L'avantage c'est que l'on peut faire toutes les modifications que l'on veut sur notre copie locale avant de les mettre à jour sur le serveur à distance. Un autre avantage est de pouvoir travailler à plusieurs (il faut juste faire attention aux *merge*).

J'ai déjà Git mais je vais vérifier de quelle version il s'agit :

![](images/../../images/module1/Git_version.JPG)

Je vais maintenant mettre à jour la configuration pour que mon *username* et mon email correspondent à ceux de GitLab.

![](images/../../images/module1/Git_configuration.JPG)

Il est également possible de faire une configuration locale (spécifique à un *repository*) en utilisant `--local` au lieu de `--global`.

Maintenant que mon Git est prêt, je vais créer une connection sécurisée entre mon ordinateur et le serveur de GitLab. Pour cela je vais utiliser un protocol SSH. 
Tout d'abord je vais checker la version du SSH sur mon ordinateur.

![](images/../../images/module1/SSH_version.JPG)

Il faut avoir une version 6.5 ou plus sinon il s'agit d'une signature MD5 qui n'est plus sécurisée.

Ce protocol SSH va nous permettre de créer une pair de clés, l'une étant privée et l'autre publique. Pour créer ces clés il faut utiliser un certain *cipher*, celui qui est conseillé est le "ED25519". Lors de la création des clés, nous devons préciser un *passphrase*[^4], il s'agit en réalité de notre mot de passe. Grâce à ce mot de passe et à leur *cipher* ils vont créer la pair de clés. Voici comment j'ai créé ces clés :

![](images/../../images/module1/SSH_key_pair.JPG)
[^4]: [Passphrase](https://www.ssh.com/academy/ssh/passphrase)

Il faut ajouter la clé publique SSH à mon compte GitLab et garder la clé privée pour soi.
Tout d'abord on copie la clé publique.

![](images/../../images/module1/SSH_copy_public_key.JPG)

Ensuite, dans les préférences de GitLab, il y a un onglet *SSH keys* où l'on peut ajouter la clé en collant le contenu.
Pour être sûre que tout a été fait correctement, je fais une vérification :

![](images/../../images/module1/SSH_verification.JPG)

Maintenant que nous avons une connection sécurisée, j'ai cloné le projet dans le *repository* que j'avais créé pour ce cours.

![](../images/module1/clone.jpg)

![](images/../../images/module1/Git_clone.JPG)

Je peux dès à présent travailler directement sur la copie du projet se trouvant sur mon ordinateur. Par contre, il faut connaître certaines commandes pour pouvoir le synchroniser avec le serveur GitLab (ce qui est possible grâce à la connection sécurisée que nous venons de créer).

| Commande | Description
| :----- | :-----
|`git pull` | Télécharger la dernière version du projet
| `git add -A` | Ajouter tous les changements que nous avons fait
| `git commit -m "message"` | Accompagner nos changements d'un message d'explication
| `git push` | Envoyer nos changement sur le serveur

### 1.2.4 Docs

Le format final de la documentation est une page web mais nous ne l'écrivons pas en HTML. Nous allons plutôt l'écrire en format markdown (plus facile à lire et à écrire) et ensuite l'exporter en HTML.

Nous allons également apprendre à insérer dans notre documentation des images et des vidéos.

J'en ai appris plus sur la documentation [ici](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown) et voici ce que j'en retiens.

#### 1.2.4.1 Éditeur de texte

J'utilise généralement _Visual Studio Code_ comme éditeur de texte. J'ai donc décidé de l'utiliser également ici. J'ai fait une recherche et j'ai trouvé ce [site](https://code.visualstudio.com/docs/languages/markdown ) qui explique quelles extensions il faut installer afin d'avoir une interface pratique pour les fichiers *markdown*.

Maintenant que j'ai les extensions nécessaires je peux facilement modifier mes fichiers ".md". Je peux également avoir un apperçu en utilisant les commandes suivantes :

- `Ctrl+Shift+v` pour ouvrir une nouvelle page contenant l'apperçu
- `Ctrl+K v` pour avoir l'apperçu à côté du document

![](images/../../images/module1/preview.JPG)

En me basant sur ces deux sites internet [markdown cheat sheet](https://www.markdownguide.org/cheat-sheet/) & [markdown formidable outil](https://www.plpeeters.com/blog/fr/post/73-le-markdown-un-formidable-outil), voici les commandes de base en markdown :

|Commande |Résultat
| :-- | :--
| `# Titre ` | ![](images/../../images/module1/titre.JPG)
| `## Sous titre ` | ![](images/../../images/module1/sous_titre.JPG)
| `### Sous sous titre ` | ![](images/../../images/module1/sous_sous_titre.JPG)
| `mot en **gras**` | mot en **gras**
| `mot en *italique*` | mot en *italique*
| `1. premier élement` \n `2. deuxième élement` | ![](images/../../images/module1/liste_numerotee.JPG)
| `- premier élement` \n `- deuxième élement` | ![](images/../../images/module1/liste.JPG)
| ![](images/../../images/module1/code_code.JPG) | `code`
| `Image : ![titre de l'image](image.jpg)` | ![titre de l'image](image)
| `Lien : [nom du lien](adresse)` | [nom du lien](adresse)
|`Ceci est le passage[^1] à annoter` | ![](images/../../images/module1/note.JPG)
|`[^1]: ceci est la note de bas de page` | ![](images/../../images/module1/bas_page.JPG)
|`- [x] tâche réalisée` \n `- [ ] tâche à faire` | ![](images/../../images/module1/tache.JPG)
|`> ceci est une citation` | ![](images/../../images/module1/citation.JPG)
|`Une définition` \n ` : le contenu de la définition` | ![](images/../../images/module1/definition.JPG)
|`| première colonne | deuxième colonne | troisième colonne` \n `| :---- | :----: | ----:` \n `| gauche | centré | droite` | ![](images/../../images/module1/tableau.JPG)
|`~~~ première ligne` \n `deuxième ligne ~~~` | ![](images/../../images/module1/code_long.JPG)

#### 1.2.4.2 Convertir en HTML

Les fichiers sources écrits en markdown sont convertis en utilisant le générateur de site *MkDocs*[^7]. Pour cela il faut un fichier de configuration écrit en YAML.

Le projet que nous recevons comporte les fichiers suivants :

- README.md
- requirements.txt
- .gitignore.txt
- mkdocs.yml
- .gitlab-ci.yml
- docs
  - FabZero-Modules
    - module01.md
    - module02.md
    - module03.md
    - module04.md
    - module05.md
    - module06.md
  - images
  - final-project.md
  - index.md

Dans le dossier *docs* se trouvent tous les fichiers de documentation.
Le fichier de configuration se nomme *mkdocs.yml* et le voici en détail :

![](images/../../images/module1/mkdocs.JPG)

|Commande | Description
| :--- | :---
|`site_name` | titre de la documentation
|`site_description` | description du site
|`site_url` | URL du site
|`repo_url` | lien vers GitLab
|`site_author` | le nom de l'auteur
|`docs_dir` | dossier contenant les fichiers sources en markdown
|`site_dir` | dossier où les fichiers HTML et autres sont créés
|`copyright` | information sur le copyright
|`theme :` | configuration du thème
|`... name` | le nom d'un thème préexistant
|`markdown_extensions :` | extensions python pour customiser le site web
|`... smarty` | extension _SmartyPants_ qui permet de convertir certains symboles ASCII en leur équivalent HTML

Pour personnaliser les informations dans le fichier mkdocs.yml. J'ai mis à jour *site_name*, *site_author*, *site_url*, *repo_url* et j'ai choisi *mkdocs* comme [thème](https://mkdocs.readthedocs.io/en/0.11.1/user-guide/styling-your-docs/).

Dans le deuxième fichier YAML, nous pouvons voir que `pip` est installé. En effet *MkDocs* utilise la librairie *Python Markdown* pour convertir les fichiers markdown en HTML. Nous pouvons également voir qu'il y a la commande `mkdocs build`, celle-ci permet de construire le site web.

![](images/../../images/module1/gitlab_yaml.JPG)

[^7]:[MkDocs](https://www.mkdocs.org/)

##### Aperçu

Afin d'avoir un aperçu direct de nos markdown, sans devoir attendre que notre site internet soit mis à jour (entre les nuits de mardi et mercredi et entre celles de mercredi et jeudi), il est possible d'utiliser MkDocs directement sur son ordinateur.

En suivant ces [instructions](https://www.mkdocs.org/getting-started/), j'ai installé MkDocs sur mon ordinateur et je peux maintenant avoir à tout moment un aperçu de mon site internet. Il suffit d'utiliser la commande `mkdocs serve`.

#### 1.2.4.3 Les images

Afin d'éviter d'utiliser trop d'espace de stockage inutilement, il est intéressant de savoir éditer nos images.

Tout d'abord, il faut savoir que JPEG est un algorithme de compression et donc les images en format JPEG perdront un peu en qualité afin de réduire leur taille et optimiser l'espace utilisé.

Ayant déjà le programme GIMP installé sur mon ordinateur, je sais comment modifier la taille de mon image ou bien convertir un PNG en JPG. Il est
également possible de récadrer, retourner, etc., l'image en suivant les instructions du [tutoriel](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg).

Pour faire ces opérations plus rapidement il est possible également de le faire sur le CLI en utilisant *GraphicsMagick*. J'ai téléchargé le fichier d'installation spécifique pour Windows et j'ai suivi les [instructions](http://www.graphicsmagick.org/INSTALL-windows.html). Grâce à la commande `gm -help` j'ai trouvé la liste des commandes.

| Commande | Description | Options
| :-- | :-- |:--
|`compare` | *compare two images* | `-size` geometry, `-type` type
| `composite` | *composite images together* | `-resize` geometry, `-background` color
|`convert` | *convert an image or sequence of images* | `-append`, `-compress` type, `-crop` geometry, `-frame` geometry, `-quality` value, `-resize` geometry
|`identify` | *describe an image or image sequence*| `-size` geometry, `-verbose`
|`montage` | *create a composite image (in a grid) from separate images* | `-adjoin`, `-compose` operator

Par exemple, j'ai converti le logo de *GraphicsMagick* avec la commande suivante : `gm convert -resize 65x65 gm.png smallgm.jpg`. Nous passons de 1.45KB à 1.41KB.
![](../images/module1/smallgm.jpg)
Par contre, le *background* transparent est devenu noir en JPG. Pour éviter cela, je le rends blanc en PNG avant de convertir avec la commande `gm convert -resize 65x65 - background white -flatten gm.png smallwhitegm.jpg`. La nouvelle image est de taille 1.27KB.
![](../images/module1/smallwhitegm.jpg)

Sur mon site, j'ai *upload* ma photo et grâce à *GraphicsMagick* j'ai pu réduire la taille du fichier d'un facteur 10 (les dimensions de la photo sont passées de 2562x2734 à 843x900).

#### 1.2.4.4 Les vidéos

Il est également possible d'insérer des vidéos dans notre documentation. Nous pourrions par exemple vouloir enregistrer notre écran, ajouter une vidéo en format mp4 ou simplement une vidéo de Youtube.

J'ai décidé d'utiliser FFMPEG pour modifier mes vidéos. Je l'ai installé en suivant ces [instructions](https://fr.wikihow.com/installer-FFmpeg-sur-Windows).

Sur le terminal j'ai utilisé la commande `ffmpeg -help` pour pouvoir consulter le manuel. Voici ce que j'en retiens :

`usage : ffmpeg [options] [[infile options] -i infile]... {[outfile options] outfile}...`

Tout d'abord pour obtenir les informations sur notre vidéo, il faut faire la commande `ffmpeg -i input.mp4 -hide_banner`. Nous obtenons la durée de la vidéo, sa taille et plein d'autres détails.

Ensuite pour diminuer la taille du fichier, on peut diminuer la résolution de notre vidéo : `ffmpeg -i input.mp4 -s 1920:1080 -c:a copy output.mp4`.

On peut également supprimer le flux audio d'une vidéo : `ffmpeg -i input.mp4 -an output.mp4`.

Afin d'afficher la vidéo dans notre documentation, j'ai du ajouté une information dans mon fichier mkdocs.yml : `plugins: - mkdocs-video`. Ensuite pour insérer la vidéo, il suffit d'écrire : `![type:video](nom_de_la_video)`. Mlaheureusement, cette option ne semble pas fonctionner. J'ai du coup décidé d'écrire directement en HTML :

```
<video controls muted>
<source src="../../images/module/video.mp4" type="video/mp4">
</video>
```

## 1.3 Checklist du module

- made a website and describe how you did it

- introduced yourself

- documented steps for uploading files to the archive

- documented steps for compressing images and keeping storage space low

- pushed to the class archive

## 1.4 Abréviations

FabLab
 : laboratoire de fabrication

CLI
 : Command Line Interface

GUI
 : Graphical User Interface

BASH
 : Bourne Again Shell

SSH
 : Secure Shell

HTML
 : HyperText Markup Language

YAML
 : Yet Another Markup Language

ASCII
 : American Standard Code for Information Interchange

PNG
 : Portable Network Graphics

JPEG
 : Joint Photographic Experts Group