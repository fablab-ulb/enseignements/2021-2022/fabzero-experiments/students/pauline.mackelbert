# 3. Découpe assistée par ordinateur

Cette semaine nous avons appris à utiliser les découpeuses laser se trouvant au Fablab ULB.

## 3.1 Précautions & matériaux

Afin d'éviter tout risque d'incendie ou sur notre santé, plusieurs [précautions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md) doivent être respectées. 

Il faut toujours : 

* activer l'air comprimé
* allumer l'extracteur de fumée
* rester à proximité de la machine jusqu'à la fin de la découpe
* savoir où est le bouton d'arrêt d'urgence
* savoir où se trouve l'extincteur au CO2

Il y a également certains matériaux qui peuvent être utilisés, et d'autres non car leurs fumées peuvent être nocives ou acides.

## 3.2 Machine 1 : Epilog fusion pro 32

Spécifications : 

- Surface de la découpe : 81x50cm
- Hauteur maximum : 31cm
- Puissance du laser : 60W

L'avantage de l'Epilog est qu'elle a une caméra qui permet de facilement positionner l'esquisse en fonction du matériau placé dans la machine. 

Les différentes étapes d'utilisations se trouvent dans ce [manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md). Il faut ouvrir l'image vectorielle et l'imprimer en choississant l'epilog en option. Cela va ouvrir le logiciel de l'epilog dans lequel on peut préciser les différents paramètres de la machine.

### 3.2.1 Les différents paramètres

**Auto Focus** : comment définir la hauteur entre le plateau et le laser

* _off_ 
* _thickness_
* _plunger_ - permet de détecter la surface du matériau

**Process type** : 

* *off*
* *Engrave* - imite les mouvements du imprimante (utile pour une image matricielle)
* *Vector*

## 3.3 Machine 2 : Lasersaur

Spécifications : 

- Surface de découpe : 122x61cm
- Hauteur maximum : 12cm
- Vitesse maximum : 6000mm/min
- Puissance du laser : 100W

Vitesse conseillée : 200mm/min

Les différentes étapes d'utilisations se trouvent dans ce [manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md). Pour cette machine il faut installer le programme DriveBoard. Ensuite, on peut ouvrir notre fichier s'il est du format DXF, SVG ou DBA.

## 3.4 Machine 3 : Full Spectrum Muse

Spécifications : 

- Surface de découpe : 50x30cm
- Hauteur maximum : 6cm
- Puissance du laser : 40W

L'avantage de la Full Spectrum Muse est qu'elle a un pointeur rouge qui facilite le positionnement de l'esquisse en fonction du matériau placé dans la machine.

Les différentes étapes d'utilisations se trouvent dans ce [manuel](https://www.slideshare.net/openp2pdesign/fab-academy-2015-laser-cutting). Pour cette machine il faut installer le programme Retina Engrave. Il faut soit se connecter via un cable ethernet ou bien via le [routeur wifi](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#mat%C3%A9riaux-pour-la-d%C3%A9coupe) dédié. Les formats de fichier compatibles avec ce programme sont SVG (vectoriel) et BMP, JPEG, PNG et TIFF (matriciels). 

Si on utilise des fichiers SVG, il faut savoir que l'esquisse sera importée en vectielle et en matricielle, donc il faudra en supprimer un des deux.

Il faut faire attention au *laser kerf*, il y a une différence entre une mesure sur l'esquisse et sur la découpe à cause de la largeur du laser.


## 3.5 Group assignment - Epilog Fusion Pro 32

Our first assignment this week was to choose a material and determine the best parameters for our specific machine (speed, power, kerf, ...)

We will work with SVG files (Scalable Vector Graphics). The program we will use is InkScape, but we could also use OpenSCAD.

### 3.5.1 Create the test bench

The first step is to sketch the piece we want to cut. We decide to do a grid of 5x5 squares on InkScape.

To be able to use it with the laser cutter, we need to use a vectorial image. So we export it this way:

![ ](../images/module3/group/vectorial.jpg)

Next, we need to color each square outline. Each one will have a different color because we want to be able to test each combination of force and speed at once.

![ ](../images/module3/group/colorsquare.jpg)

We chose to use a gray gradient for our different colors.

We export our sketch into SVG. Using a USB key we open our file on the Epilog's computer.

### 3.5.2 Cut the test bench (first try)

We open the file and we go to *FILE>PRINT* and we select the machine.

![ ](../images/module3/group/fileprint.jpg)

Next, we get into the Epilog's Software. We need now to set the parameters.

First, pushing on the sketch, we have the possibility to split this sketch by colors. Then, we can determine the parameters for each subpart (each part of the sketch having a different color).

For each one, we choose those parameters :

- **process type** : *vector*
- **speed** : we go from 10 to 90 (step of 20)
- **power** : we go from 10 to 90 (step of 20)

Next, we choose to go with orange cardboard. We put it into the laser cutter (careful to put it flat in the machine to keep a constant distance between the cardboard and the laser).

On the software, we check with the video how to place the sketch. After, we check that the borders are well placed (those borders defines the region the laser can go into).

We choose for the parameter **Auto Focus** : *plunger*. Then, we *print* to add the file to the Epilog machine.

Comme nous pouvons le voir sur la photo suivante, nous avons allumé l'extracteur de fumée, nous avons activé la machine grâce à la clé, nous avons bien fermé la machine en appuyant dessus et en vérifiant que les LED correspondantes étaient bien allumées. 

![ ](../images/module3/group/careful.jpg)

We choose the file on the Epilog machine and we start.

![ ](../images/module3/group/start.jpg)

After having cut the piece, we can delete the file from the machine.

![ ](../images/module3/group/test1.jpg)

The result is a bit disappointing, almost every square is simply totally cut through. It is simply because the cardboard is too thin and the laser set on a too high power.

### 3.5.3 Cut the test bench (second try)

For our second try, we decided to focus ourself on a small part of the power range.

![ ](../images/module3/group/drawing.jpg)

This time, the parameters are :

- **process type** : *vector*
- **speed** : we go from 30 to 90 (step of 15)
- **power** : we go from 5 to 25 (step of 5)

![ ](../images/module3/group/test2.jpg)

The result is much more conclusive. Some of the square were very loose and fell down as soon as we picked up the test bench, but we have much more samples to study. We can see that if we go too slow, it will inevitably cut through the paper.

### 3.5.4 cut the test bench (third try)

For the second part of the assignment, the Kirigamis, we still lack one important information, what is are the best parameters to be able to easily fold our cardboard without simply cutting it.

We designed a simple rectangle with 5 lines, each having different parameters. We chose those parameters based on our previous test bench, we selected the squares that were not cut through but still quite deeply cut.

![ ](../images/module3/group/PLI.jpg)

The parameters are :

- **Process type** : *vector*
- **Speed** : 30, 60, 75, 90, 90
- **Power** : 5, 10, 10, 10, 15

Every cut hangs on correctly, but the best parameters pair seems to be (90,10), as it seems to be a bit more resilient.

## 3.6 Personal assignment

Pour mon projet personnel, j'ai décidé de créer un cube. 

### 3.6.1 Esquisse

Tout d'abord il faut que je crée l'esquisse de mon cube. J'ai décidé d'utiliser OpenSCAD pour pouvoir paramétriser les dimensions de mon cube. 

```
/*
* 
* FILE : cube_construction
*
* AUTHOR : Pauline Mackelbert
*
* DATE : 11/03/2022
*
* LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
*/

size = 40;

cube_construction(size);

module cadre(size)
{
    difference(){
        square(size,center=true);
        square(size-0.1,center=true);}
    };

module cube_construction(size)
{
    union(){
        cadre(size);
        translate([size-0.05,0,0])cadre(size);
        translate([2*(size-0.05),0,0])cadre(size);
        translate([3*(size-0.05),0,0])cadre(size);
        translate([0,size-0.05,0])cadre(size);
        translate([0,-size+0.05,0])cadre(size);}
    }
```

![](../images/module3/esquisse.JPG)

Ensuite, pour pouvoir ouvrir cette esquisse dans Inkscape, je dois l'exporter. Je l'ai d'abord exporté en SVG, seulement je n'arrivais à pouvoir considérer chaque arrête séparemment. Je l'ai du coup exporté en DFX.

Comme, avec ce format, je peux sélectionner une arrête à la fois, j'ai défini leurs couleurs.

![](../images/module3/construction_color.JPG)

Les arrêtes rouges seront découpées et les arrêtes vertes seront gravées pour faciliter le pliage.

### 3.6.2 Découpe

Afin de faciliter mon travail, j'ai décidé d'utiliser le même matériau que pendant le travail de groupe (carton orange). 

Selon nos recherches, nous avons défini que pour graver et facilité le pliage les meilleurs paramètres sont : 

- *Speed* : 90
- *Power* : 10 

Pour les paramètres de découpe, j'ai décidé de prendre : 

- *Speed* : 30
- *Power* : 15

En suivant toutes les instructions que nous avons déjà détaillées dans le travail de groupe, j'ai ouvert le fichier sur l'ordinateur de l'Epilog, j'ai *split by color* pour pouvoir définir les paramètres pour chaque couleur, j'ai fait toutes les mesures de sécurité et j'ai positionné correctement mon esquisse grâce à la caméra. 

Seulement pour mon premier essai j'ai attribué les paramètres de pliage et de découpe aux mauvaises couleurs. J'ai donc obtenu l'inverse de ce que je souhaitais.

![](../images/module3/decoupe1.jpg)

Par contre, en encodant correctement les paramètres, le résultat était comme souhaité : les bords sont découpés proprement et les différentes parties se plient sans se couper.

![](../images/module3/decoupe2.jpg)

## 3.7 Checklist du module

- Explained what you learned from testing the lasercutters

- Explained how you designed your CAD files

- Documented how you made your kirigami and 3D object

- Included your original design files

- Included your hero shots