/* 
 *  FILE : Project2_SOS_Distress_Signal
 *  
 *  AUTHOR : Pauline Mackelbert
 *  
 *  DATE : 24/03/2022
 *  
 *  FROM : https://wiki.dfrobot.com/DFRduino_Beginner_Kit_For_Arduino_V3_SKU_DFR0100
 */

 int ledPin = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin,OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  //3 quick blinks to represent "S"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH);
    delay(150);
    digitalWrite(ledPin,LOW);
    delay(100);
  }

  //break
  delay(100);

  //3 blinks to represent "0"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH);
    delay(400);
    digitalWrite(ledPin,LOW);
    delay(100);
  }

  //break
  delay(100);

  //3 quick blinks to represent "S"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH);
    delay(150);
    digitalWrite(ledPin,LOW);
    delay(100);
  }

  //wait 5 seconds to repeat the next signal
  delay(5000);

  
  

}
