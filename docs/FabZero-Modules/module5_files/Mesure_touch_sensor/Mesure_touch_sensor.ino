/* 
 *  FILE : Mesure_touch_sensor
 *  
 *  AUTHOR : Pauline Mackelbert
 *  
 *  DATE : 06/04/2022
 *  
 *  FROM : https://arduinogetstarted.com/tutorials/arduino-touch-sensor
 */

const int SENSOR_PIN = 7;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(SENSOR_PIN,INPUT);
}

void loop() {
  // read the state of the input pin:
  int state = digitalRead(SENSOR_PIN);

  // print state to Serial Monitor
  Serial.println(state);
}
