/*
 * FILE : Project3_Interactive_Traffic_Lights
 * 
 * AUTHOR : Pauline Mackelbert
 * 
 * DATE : 24/03/2022
 * 
 * FROM : https://wiki.dfrobot.com/DFRduino_Beginner_Kit_For_Arduino_V3_SKU_DFR0100
 */

 int carRed = 12;
 int carBlue = 11;
 int carGreen = 10;
 int button = 9;
 int pedRed = 8;
 int pedGreen = 7;

 int crossTime = 5000; // time for pedestrians to pass unsigned
 long changeTime; // time that the button is pressed

void setup() {
  // put your setup code here, to run once:
  // configure all LEDs as output
  pinMode(carRed,OUTPUT);
  pinMode(carBlue,OUTPUT);
  pinMode(carGreen,OUTPUT);
  pinMode(pedRed,OUTPUT);
  pinMode(pedGreen,OUTPUT);
  // configure button as input
  pinMode(button,INPUT);
  // initialize car and pedestrian traffic lights
  digitalWrite(carGreen,HIGH);
  digitalWrite(pedRed,HIGH);

}

void loop() {
  // put your main code here, to run repeatedly:
  //test if the button is pressed and if 5 seconds have passed after it is pressed
  int state = digitalRead(button);
  if(state == HIGH && (millis()-changeTime)>5000){
    changeLights();
  }
}

void changeLights(){
  // car traffic light to blue
  digitalWrite(carGreen,LOW);
  digitalWrite(carBlue,HIGH);
  delay(2000);
  // car traffic light to red
  digitalWrite(carBlue,LOW);
  digitalWrite(carRed,HIGH);
  delay(1000);
  // pedestrian light to green
  digitalWrite(pedRed,LOW);
  digitalWrite(pedGreen,HIGH);
  delay(crossTime);
  // blink green pedestrian light to notify pedestrians to pass soon
  for (int x=0;x<10;x++){
    digitalWrite(pedGreen,HIGH);
    delay(250);
    digitalWrite(pedGreen,LOW);
    delay(250);
  }
  // pedestrian light to red
  digitalWrite(pedRed,HIGH);
  delay(500);
  // car traffic light to blue
  digitalWrite(carRed,LOW);
  digitalWrite(carBlue,HIGH);
  delay(1000);
  // car traffic light to green
  digitalWrite(carBlue,LOW);
  digitalWrite(carGreen,HIGH);
  // record the duration since last change
  changeTime = millis();
}
