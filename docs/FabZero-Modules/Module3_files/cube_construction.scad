/*
* 
* FILE : cube_construction
*
* AUTHOR : Pauline Mackelbert
*
* DATE : 11/03/2022
*
* LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
*/

size = 40;

cube_construction(size);

module cadre(size)
{
    difference(){
        square(size,center=true);
        square(size-0.1,center=true);}
    };

module cube_construction(size)
{
    union(){
        cadre(size);
        translate([size-0.05,0,0])cadre(size);
        translate([2*(size-0.05),0,0])cadre(size);
        translate([3*(size-0.05),0,0])cadre(size);
        translate([0,size-0.05,0])cadre(size);
        translate([0,-size+0.05,0])cadre(size);}
    }