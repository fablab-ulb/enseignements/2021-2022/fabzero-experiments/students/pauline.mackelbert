/*

FILE : flexlink_lego_3.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Alexandre Stoesser, Flexible3Dv2.scad last modified on 22/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/blob/main/docs/Files/Flexible3Dv2.scad)

LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

*/

$fn=50;

height = 3;
middle_length = 30;
center_holes_distance = 8;
structure_length = 18;
structure_width = 3.4*2;
diameter_hole = 2.5*2;

epaisseur = height;
longueur_de_la_tige =middle_length;
distance_entre_les_trous = center_holes_distance;
longueur_du_bloc = structure_length;
largeur_du_bloc = structure_width;
largeurtrou = diameter_hole;


largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;



width=1; depth = longueur_de_la_tige;


x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0; 

x1=0; y1=y0+distance_entre_les_trous/2; z1=0; 

x2=0; y2=y0-distance_entre_les_trous/2; z2=0; 

union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);
    
     
}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){
    
 

    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){
    
    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}
