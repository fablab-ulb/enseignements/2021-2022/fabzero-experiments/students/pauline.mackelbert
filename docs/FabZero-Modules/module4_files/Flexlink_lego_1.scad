/*

File : Flexlink_lego_1.scad

Author : Pauline Mackelbert

Date : 16 mars 2022

License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

*/

//parameters
radius_part = 3.4;
radius_hole = 2.5;
height = 3;
distance_cylinder = 8;
distance_part = 25;

$fn=100;
flexlinks(height,radius_part,radius_hole,distance_cylinder,distance_part);

module doublecylinder(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    //create two cylinders
    union(){
        cylinder(h=height,r=radius,center=true);
        translate([distance,0,0])cylinder(h=height,r=radius,center=true);
    }
}

module largecylinder(height,radius,distance_cylinder)
{
    //hull the two cylinders
    hull(){
        doublecylinder(height,radius,distance_cylinder);
    }      
}

module part(height,radius_part,radius_hole,distance_cylinder)
{
    //remove the two cylinders to have holes
    difference(){
        largecylinder(height,radius_part,distance_cylinder);
        doublecylinder(height,radius_hole,distance_cylinder);
    }
}

module doublepart(height,radius_part,radius_hole,distance_cylinder,distance_part)
{
    //create two parts
    union(){
        part(height,radius_part,radius_hole,distance_cylinder);
        translate([0,distance_part,0])part(height,radius_part,radius_hole,distance_cylinder);
    }
}

module arc(distance_part,height,radius_part,radius_hole)
{
    difference(){
        difference(){
            translate([-radius_part,distance_part/2,0])cylinder(h=height,d=distance_part,center=true);
            translate([-radius_part,distance_part/2,0])cylinder(h=height,d=distance_part-1,center=true);
        }
    translate([-radius_part+distance_part/4,distance_part/2,0])cube([distance_part/2,distance_part,height],center=true);    
    }
}

module flexlinks(height,radius_part,radius_hole,distance_cylinder,distance_part)
{
    union(){
        doublepart(height,radius_part,radius_hole,distance_cylinder,distance_part);
        arc(distance_part,height,radius_part,radius_hole);
    }
}    