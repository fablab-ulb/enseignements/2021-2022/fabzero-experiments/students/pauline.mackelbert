/*

File : Flexlinks_lego_2_3.scad

Author : Pauline Mackelbert

Date : 30 mars 2022

License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

*/

//parameters
radius_part = 3.4;
radius_hole = 2.5;
height = 3;
distance_cylinder = 8;
distance_part = 25;

$fn=100;

/* 
Flexlink_lego_2

FILE : flexlink_lego_2.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Nathalie Wéron, ovni_final.scad last modified on 24/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/blob/main/docs/FabZero-Modules/CAD/ovni_final.scad)
*/

middle_length = 90;
middle_thickness = 1;

h = height;
d = middle_length;
w = middle_thickness;
rc = radius_part;
e = distance_cylinder;
hole = radius_hole;

translate([distance_cylinder*3,0,0])
difference(){
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])cube([w,d,h]);
        
        //extremity 1
        cylinder(h, r=rc);
        translate([0,e,0])cylinder(h, r=rc);
        translate([-rc,0,0])cube([2*rc,e,h]);
        
        //extremity 2
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])cylinder(h, r=rc);
            translate([-rc,0,0])cube([2*rc,e,h]);
        }
        
        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])cylinder(h, r=rc);
                translate([-rc,0,0])cube([2*rc,3*e,h]);
            }
            
            //extremity1_body
            translate([0,-(e+e/2),0]){
                rotate([0,0,45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,-2*e,0])cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])cylinder(h,r=rc);
                }
            }
            
            //extremity2_body
            translate([0,e+e/2,0]){
                rotate([0,0,-45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,0,0])cube([2*rc,2*e,h]);
                    translate([0,2*e,0])cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES
    
    //extremity 1_holes
    translate([0,0,-1])cylinder(h+2, r=hole);
    translate([0,e,-1])cylinder(h+2, r=hole);
    
    //extremity 2_holes
    translate([0,d+(2*rc+e),0]){
        translate([0,0,-1])cylinder(h+2, r=hole);
        translate([0,e,-1])cylinder(h+2, r=hole);
    }
    
    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),-1]){ //put at centre
                cylinder(h+2, r=hole);
                translate([0,e,0])cylinder(h+2, r=hole);
                translate([0,2*e,0])cylinder(h+2, r=hole);
                translate([0,3*e,0])cylinder(h+2, r=hole);
                translate([-hole,e,0])cube([2*hole,e,h+2]);
            }
            
            //extremity1_holes
            translate([0,-(e+e/2),0]){//same as body
                rotate([0,0,45]){//same as body
                    translate([0,-e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,-2*e,-1])cylinder(h+2,r=hole);
                }
            }
         
            //extremity2_holes
            translate([0,e+e/2,0]){ //same as body
                rotate([0,0,-45]){ //same as body
                    translate([0,e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,2*e,-1])cylinder(h+2,r=hole);
                }
            }
        }
}

/*
Flexlink_lego_3

FILE : flexlink_lego_3.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Alexandre Stoesser, Flexible3Dv2.scad last modified on 22/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/blob/main/docs/Files/Flexible3Dv2.scad)
*/

middle_length2 = 30;
structure_length = 18;

epaisseur = height;
longueur_de_la_tige =middle_length2;
distance_entre_les_trous = distance_cylinder;
longueur_du_bloc = structure_length;
largeur_du_bloc = radius_part*2;
largeurtrou = radius_hole*2;

largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;

width=1; depth = longueur_de_la_tige;

x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0; 
x1=0; y1=y0+distance_entre_les_trous/2; z1=0; 
x2=0; y2=y0-distance_entre_les_trous/2; z2=0; 

translate([distance_cylinder*8 ,middle_length2,height/2])
union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);   
}

module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){
    
    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){
    
    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}