/*

FILE : flexlink_lego_2.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Nathalie Wéron, ovni_final.scad last modified on 24/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/blob/main/docs/FabZero-Modules/CAD/ovni_final.scad)

LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

*/

$fn = 50;
height = 3;
middle_length = 90;
middle_thickness = 1;
radius_part = 3.4;
radius_hole = 2.5;
center_holes_distance = 8;

h = height;
d = middle_length;
w = middle_thickness;
rc = radius_part;
e = center_holes_distance;
hole = radius_hole;

difference(){
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])cube([w,d,h]);
        
        //extremity 1
        cylinder(h, r=rc);
        translate([0,e,0])cylinder(h, r=rc);
        translate([-rc,0,0])cube([2*rc,e,h]);
        
        //extremity 2
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])cylinder(h, r=rc);
            translate([-rc,0,0])cube([2*rc,e,h]);
        }
        
        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])cylinder(h, r=rc);
                translate([-rc,0,0])cube([2*rc,3*e,h]);
            }
            
            //extremity1_body
            translate([0,-(e+e/2),0]){
                rotate([0,0,45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,-2*e,0])cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])cylinder(h,r=rc);
                }
            }
            
            //extremity2_body
            translate([0,e+e/2,0]){
                rotate([0,0,-45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,0,0])cube([2*rc,2*e,h]);
                    translate([0,2*e,0])cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES
    
    //extremity 1_holes
    translate([0,0,-1])cylinder(h+2, r=hole);
    translate([0,e,-1])cylinder(h+2, r=hole);
    
    //extremity 2_holes
    translate([0,d+(2*rc+e),0]){
        translate([0,0,-1])cylinder(h+2, r=hole);
        translate([0,e,-1])cylinder(h+2, r=hole);
    }
    
    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),-1]){ //put at centre
                cylinder(h+2, r=hole);
                translate([0,e,0])cylinder(h+2, r=hole);
                translate([0,2*e,0])cylinder(h+2, r=hole);
                translate([0,3*e,0])cylinder(h+2, r=hole);
                translate([-hole,e,0])cube([2*hole,e,h+2]);
            }
            
            //extremity1_holes
            translate([0,-(e+e/2),0]){//same as body
                rotate([0,0,45]){//same as body
                    translate([0,-e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,-2*e,-1])cylinder(h+2,r=hole);
                }
            }
         
            //extremity2_holes
            translate([0,e+e/2,0]){ //same as body
                rotate([0,0,-45]){ //same as body
                    translate([0,e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,2*e,-1])cylinder(h+2,r=hole);
                }
            }
        }
}





