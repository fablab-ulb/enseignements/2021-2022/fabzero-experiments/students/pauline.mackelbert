# 5. Electronique 1 - Prototypage

Cette semaine nous avons appris à utiliser un microcontrôleur afin de prototyper des applications électroniques.

Microcontrôleur [^1]
: "un microcontrôleur est un circuit intégré rassemblant dans un même boitier un microprocesseur, plusieurs types de mémoires et des périphériques de communication."

[^1]: [définition microcontrôleur](https://www.techno-science.net/definition/6737.html)

Arduino [^2]
: "Arduino est un ensemble matériel et logiciel qui permet d'apprendre l'électronique (en s'amusant) tout en se familiarisant avec la programmation informatique. Arduino est open source."

[^2]: [définition Arduino](https://www.positron-libre.com/electronique/arduino/arduino.php)

## 5.1 *Arduino board*

L'avantage d'Arduino comparé à un autre microcontrôleur est que tout est intégré dans un seul environnement : le dispositif de programmation sur la carte Arduino, le logiciel Arduino IDE pour écrire notre code, pour l'envoyer sur la carte et le tester.

Il existe une multitude de [modules I/O](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/07e9c48531529f84506c966be76957e4dba6ec1c/IO-Modules.md) (*Input/Output*) qui peuvent être utilisés avec la carte Arduino.

| I/O | Type | Détails
| :-- | :-- | :--
| Entrée | Interrupteurs | interrupteur, capteur de vibrations, ...
| Entrée | Capteurs analogiques | capteur de température analogique, capteur capacitif, capteur de courant, capteur de fréquence cardiaque
| Entrée | Détecteurs | détecteur d'himidité du sol, détecteur de niveau d'eau, détecteur sonore avec microphone, capteur de mouvement, ...
| Entrée | Capteurs numériques | capteur de couleur, capteur de température, accéléromètre numérique, capteur de temps de vol
| Sortie | Optiques | module de transmission infrarouge, module LED RGB, écran OLED, afficheur 4 digits
| Sortie | Électro-mécanique / Audio | module relais 5V, électro-aimant, module buzzer
| Sortie | Interface de puissance | module de pilotage MOS, module contrôleur de moteur

Mais que veut dire analogique et numérique. Un signal analogique est "une onde continue qui change sur une période de temps" et un signal numérique est "une onde discrète qui transporte des informations sous forme binaire" [^3].

[^3]: [numérique vs analogique](https://waytolearnx.com/2018/07/difference-entre-le-signal-analogique-et-numerique.html)

![](../images/module5/Difference_entre_le_signal_analogique_et_numerique.JPG)

### 5.1.1 Arduino UNO

Voici le schéma *pinout de l'arduino UNO. Celui-ci nous permet de connaître la spécification de chaque pin présent sur la carte.

![](../images/module5/Arduino-uno-pinout.JPG)

Il y a des pins réservés pour les signaux *digital* et ceux *analog*. Les signaux *digital* basculent entre les valeurs binaires 0 et 1 qui correspondent ici à 0V et 5V.

La connectivé entre les pins et les voltages ainsi qu'au *ground* varie en fonction qu'un pin soit une entrée ou une sortie. Si nous ne définissons pas correctement le comportement des pins alors nous risquons un court circuit et de faire brûler notre carte Arduino.

Certains pins sont PWN (*Pulse Wire Modulation*), ceux-ci permettent de simuler un signal analogique à partir d'un signal numérique.

Pour plus de détails sur chaque pin, nous pouvons regarder ce [manuel](https://www.circuito.io/blog/arduino-uno-pinout/).

## 5.2 Arduino IDE

J'ai installé l'application [Arduino IDE](https://www.arduino.cc/en/software) pour Windows.

### 5.2.1 Fonctions

Il existe tout plein de fonctions pour contrôler la carte Arduino. En voici déjà quelques unes de base, et les autres peuvent être retrouvées dans cette [liste](https://www.arduino.cc/reference/en/).

#### *Digital I/O*

| Fonction | Détails | Signification
| :-- | :-- | :--
| [`pinMode(pin,mode)`](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/) | `mode` : `OUTPUT`, `INPUT` | configure le comportement des pins soit comme une entrée, soit comme une sortie
| [`digitalWrite(pin,value)`](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/) | `value` : `HIGH`, `LOW` | écrit une valeur *high* (on) ou *low* (off) sur un pin numérique (pour les pins de sortie)
| [`digitalRead(pin)`](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/) | | lit la valeur d'un pin numérique, soit *high* soit *low* (pour les pins d'entrée)

#### *Analog I/O*

| Fonction | Détails | Signification
| :-- | :-- | :--
| [`analogRead(pin)`](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) | | lit la valeur d'un pin analogique (pour les pins d'entrée)
| [`analogReference(type)`](https://www.arduino.cc/reference/en/language/functions/analog-io/analogreference/) | `type` : `DEFAULT`, etc | configure la tension de référence utilisée pour l'entrée analogique
| [`analogWrite(pin, value)`](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/) | `value` : entre 0 (toujours off) et 255 (toujours on)| écrit une valeur analogique (onde PWN) sur un pin (pour les pins de sortie)

#### *Time*

| Fonction | Détails | Signification
| :-- | :-- | :--
| [`delay(time)`](https://www.arduino.cc/reference/en/language/functions/time/delay/) | `time` in milliseconds | met le programme en pause pendant la durée spécifiée

### 5.2.2 Code

Les éléments du code Arduino (en C++) se nomment des structures et on peut les retrouver [ici](https://www.arduino.cc/reference/en/#structure).

## 5.3 Assignment

### 5.3.1 Exercices simples

Je me suis aidée de ce *Beginner kit for Arduino tutorial* que l'on peut trouver [ici](https://www.dfrobot.com/product-345.html).

#### 5.3.1.1 Allumer une LED

J'ai décidé de commencer par allumer et d'éteindre une LED  (Light-Emitted Diode) chaque seconde.

 Voici mon code :

```
/* 
 * FILE : Project1_LED_Flashing 
 * 
 * AUTHOR : Pauline Mackelbert
 * 
 * DATE : 24/03/2022
 *
 *FROM : https://wiki.dfrobot.com/DFRduino_Beginner_Kit_For_Arduino_V3_SKU_DFR0100
 */

 int ledPin = 10; // Choix du pin auquel est connecté la LED

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin,OUTPUT); // Il s'agit d'une LED donc il faut configurer le pin comme une sortie
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin,HIGH); // ON donc on allume la LED
  delay(1000); // on attend 1 seconde
  digitalWrite(ledPin,LOW); // OFF donc on éteint la LED
  delay(1000); // on attend 1 seconde
}
```

Pour réaliser cela j'ai fait le câblage suivant :

![](../images/module5/cablage_project1.JPG)[^4]

[^4]: [Beginner Kit for Arduino](https://github.com/DFRobot/Beginner-Kit-for-Arduino/blob/master/Beginner%20Kit%20for%20Arduino%20Tutorial.pdf)

Les LEDs sont polarisées donc elles doivent être connectées dans le bon sens.
Les LEDs ont également besoin de 2 V de tension et de 35 mA de courant. Pour avoir cela il faut mettre insérer une résistance. En effet les résistances résistent au courant donc plus la résistance est forte moins de courant passe à travers. Il faut donc au minimum une résistance de 100 Ohm pour une LED. Ici nous utilisons une résistance de 220 Ohm.


Avant de connecter mon ordinateur à la carte Arduino, j'ai d'abord vérifié dans `Tools > Board` qu'il s'agissait du bon modèle, c'est à dire "Arduino Uno". Ensuite j'ai `Verify` mon code pour m'assurer qu'il n'y ait pas d'erreur. Finalement, j'ai connecté la carte Arduino et j'ai `Upload` le code. Voici le résultat :

<video controls muted>
<source src="../../images/module5/video_project1.mp4" type="video/mp4">
</video>

#### 5.3.1.2 SOS en morse

Cette fois-ci j'ai décidé de dire SOS en morse en allumant et éteignant une LED. Voici mon code :

```
/* 
 *  FILE : Project2_SOS_Distress_Signal
 *  
 *  AUTHOR : Pauline Mackelbert
 *  
 *  DATE : 24/03/2022
 *  
 *  FROM : https://wiki.dfrobot.com/DFRduino_Beginner_Kit_For_Arduino_V3_SKU_DFR0100
 */

 int ledPin = 10; // la LED est connectée au pin 10

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin,OUTPUT); // nous définissons donc ce pin comme une sortie

}

void loop() {
  // put your main code here, to run repeatedly:

  //3 clignotements rapides pour représenter "S"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH); 
    delay(150);
    digitalWrite(ledPin,LOW);
    delay(100);
  }
   
  //Break entre les lettres
  delay(100);

  //3 clignotements pour représenter "O"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH);
    delay(400);
    digitalWrite(ledPin,LOW);
    delay(100);
  }

  //break entre les lettres
  delay(100);

  //3 clignotements rapides pour représenter "S"
  for(int x=0;x<3;x++) {
    digitalWrite(ledPin,HIGH);
    delay(150);
    digitalWrite(ledPin,LOW);
    delay(100);
  }

  //Attendre 5 secondes avant de recommencer
  delay(5000);
}
```

Pour réaliser cela j'ai fait utiliser le même cablage que précédemment.

Voici le résultat :

<video controls muted>
<source src="../../images/module5/video_project2.mp4" type="video/mp4">
</video>

### 5.3.2 Mesurer quelque chose

J'ai décidé d'utiliser un *touch sensor* ([datasheet](https://files.seeedstudio.com/wiki/Grove-Touch_Sensor/res/TTP223.pdf)).

![](../images/module5/cablage_mesure.jpg)[^5]

[^5]: [touch sensor](https://arduinogetstarted.com/tutorials/arduino-touch-sensor)

Celui-ci est calibré (précisé dans la datasheet). Je vais quand même vérifié que le senseur fonctionne correctement.

```
/* 
 *  FILE : Mesure_touch_sensor
 *  
 *  AUTHOR : Pauline Mackelbert
 *  
 *  DATE : 06/04/2022
 *  
 *  FROM : https://arduinogetstarted.com/tutorials/arduino-touch-sensor
 */

const int SENSOR_PIN = 7;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(SENSOR_PIN,INPUT);
}

void loop() {
  // read the state of the input pin:
  int state = digitalRead(SENSOR_PIN);

  // print state to Serial Monitor
  Serial.println(state);
}
```

Voici ce que ça donne dans le *serial monitor* :

![](../images/module5/mesure.JPG)

Nous voyons qu'en l'espace d'une seconde, en touchant très vite le senseur, nous passons bien de l'état 0 à l'état 1 pour revenir à l'état 0.

### 5.3.3 Faire faire quelque chose à notre carte Arduino

J'ai décidé de simuler un feu de signalisation en utilisant des LEDs rouges, vertes et bleues (le bleu remplace le jaune d'un feu de signalisation standard). En utilisant le senseur de touché nous allons simuler un feu de signalisation à la fois pour les voitures et pour les piétons. Lorsque les piétons touchent le boitier sur le feu, le feu pour les voitures devient rouge et le feu pour les piétons devient vert.

```
/*
 * FILE : Project3_Interactive_Traffic_Lights
 * 
 * AUTHOR : Pauline Mackelbert
 * 
 * DATE : 24/03/2022
 * 
 * FROM : https://wiki.dfrobot.com/DFRduino_Beginner_Kit_For_Arduino_V3_SKU_DFR0100
 */

 int carRed = 12; // LED rouge pour les voitures sur pin 13
 int carBlue = 11; // LED bleue pour les voiture sur pin 11
 int carGreen = 10; // LED verte pour les voitures sur pin 8

 int button = 9; // bouton sur pin 2

 int pedRed = 8; // LED rouge pour les piétons sur pin 7
 int pedGreen = 7; // LED verte pour les piétons sur pin 5

 

 int crossTime = 5000; // le temps que les piétons ont pour traversés une fois avoir touché le boitier
 long changeTime; // moment où le boitier a été touché

void setup() {
  // put your setup code here, to run once:
  // Configurer toutes les LEDs comme des sorties
  pinMode(carRed,OUTPUT);
  pinMode(carBlue,OUTPUT);
  pinMode(carGreen,OUTPUT);
  pinMode(pedRed,OUTPUT);
  pinMode(pedGreen,OUTPUT);
  // Configurer le bouton comme une entrée
  pinMode(button,INPUT);
  // Initialiser les feux de signialisation (voitures et piétons)
  digitalWrite(carGreen,HIGH); // Le feu est vert pour les voitures
  digitalWrite(pedRed,HIGH); // Le feu est rouge pour les piétons

}

void loop() {
  // put your main code here, to run repeatedly:
  // Check si le boitier est touché et s'il y a eu 5 secondes qui se sont écoulées depuis la dernière fois
  int state = digitalRead(button);
  if(state == HIGH && (millis()-changeTime)>5000){
    changeLights();
  }
}

void changeLights(){
  // Le feu passe du vert au bleu pour les voitures pour les prévenir de ralentir
  digitalWrite(carGreen,LOW);
  digitalWrite(carBlue,HIGH);
  delay(2000);
  // Le feu passe du bleu au rouge pour les voitures pour qu'elles s'arretent
  digitalWrite(carBlue,LOW);
  digitalWrite(carRed,HIGH);
  delay(1000);
  // Le feu passe du rouge au vert pour les piétons, ils peuvent traverser
  digitalWrite(pedRed,LOW);
  digitalWrite(pedGreen,HIGH);
  delay(crossTime); // attendre les 5 secondes
  // Faire clignoter le feu vert pour les piétons pour les prévenir qu'il va bientôt devenir rouge
  for (int x=0;x<10;x++){
    digitalWrite(pedGreen,HIGH);
    delay(250);
    digitalWrite(pedGreen,LOW);
    delay(250);
  }
  // Le feu passe du vert au rouge pour les piétons
  digitalWrite(pedRed,HIGH);
  delay(500);
  // Le feu passe du rouge au bleu pour les voitures
  digitalWrite(carRed,LOW);
  digitalWrite(carBlue,HIGH);
  delay(1000);
  // Le feu passe du bleu au vert pour les voitures, elles peuvent démarrer
  digitalWrite(carBlue,LOW);
  digitalWrite(carGreen,HIGH);
  // Enregistrer le moment où le changement a eu lieu
  changeTime = millis();
}
```

Pour réaliser cela j'ai fait le câblage suivant :

![](../images/module5/cablage_project3.JPG)[^6]

[^6]: [Beginner Kit for Arduino Tutorial](https://github.com/DFRobot/Beginner-Kit-for-Arduino/blob/master/Beginner%20Kit%20for%20Arduino%20Tutorial.pdf)

![](../images/module5/cablage_traffic_light.jpg)

Voici le résultat :

<video controls muted>
<source src="../../images/module5/video_project3.mp4" type="video/mp4">
</video>

## 5.4 Checklist du module

- Documented what you have learned from interfacing an input device to a micro controller and how the physical property relates to the measured results

- (optional) Documented what you have learned from interfacing an output to a micro controller and controlling the device

- Programmed your board

- Described your programming process

- Outlined problems and how you fixed them

- Included original design files and source code

- Included a "hero shot/video" of your board