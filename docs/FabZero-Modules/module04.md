# 4. Impression 3D

Cette semaine nous avons appris à utiliser les imprimantes 3D Prusa ainsi que leur logiciel PrusaSlicer.

## 4.1 Introduction

Goole Jonathan, Professeur de la Faculté de Pharmacie de l'ULB, nous a présenté à quoi pouvait servir l'impression 3D dans différents domaines d'applications. En effet, celle-ci est utilisée dans les domaines suivants :

- santé (avantage : permet d'avoir des traitements individualisés)
- agroalimentaire
- construction
- automobile
- biens de consommation
- aéronautique

L'impression 3D permet de fabriquer des pièces très complexes dans des matériaux intéressants (permettent de réduire le poid et le coût des structures imprimées).

L'impression 3D peut également être adaptée à notre idée de projet, et en fonction de celle-ci on choisit la méthode d'impression appropriée.

Il existe donc différentes méthodes d'impression 3D.

- Impression 3D sur lit de poudre

- Impression 3D goutte-sur-goutte

- Impression 3D par micro seringues pressurisées (utilisé en cuisine pour faire des gâteaux majestueux)

- Impression 3D par dépôt du fondu (celles du Fablab ULB)

- Stéréolithographie (SLA)

- Digital light processing (DLP)

## 4.2 Impression 3D par dépôt du fondu (*fused-deposition modeling* FDM)

Ces imprimantes 3D utilise des bobines de filaments thermoplastiques. Ces filaments doivent pouvoir rapidement fondre et se resolidifier, il y a peu de matériaux comme cela et donc peu de [matériaux disponibles](https://help.prusa3d.com/en/category/material-guide_220) pour les bobines de filaments.

### 4.2.1 Pré-traitement

Ces filaments doivent être pré-traités car il ne doit plus y a avoir d'eau du tout sinon cela forme des bulles ce qui crée des structures poreuses. Il faut donc sécher les filaments (dans un four spécialisé ou un four de maison).

Il faut également mettre le plateau à niveau. Pour cela il faut d'abord nettoyer la tête d'impression et le plateau de construction puis de régler la hauteur entre la tête et le plateau.

### 4.2.2 Post-traitement

Si nous avons utilisé des supports (*raft*) pour créer des structures complexes, il faut les retirer (soit mécaniquement ou bien dans de l'eau si les supports sont solubles).

Afin d'avoir une surface lisse, il faut poncer la structure ou bien utiliser un traitement à la vapeur.

## 4.3 Utilisation des imprimantes du Fablab ULB

Il s'agit donc d'imprimantes 3D FDM Prusa. Ce sont des imprimantes qui ont été majoritairement imprimées. Les fichiers sont open source et cela permet de pouvoir les réparer si besoin.

Caractéristiques des Prusa i3 MK3s :

- Taille du plateau d'impression : 22,5cmx22,5cm
- Hauteur d'impression maximale : 25cm
- Diamètre de buse : 0,4mm (au FabLab ULB)

Pour pouvoir imprimer sur les imprimantes Prusa il faut d'abord télécharger PrusaSlicer.

### 4.3.1 Installation de PrusaSlicer

*Slicing software*[^1]
: "Computer software used in the majority of 3D printing processes for the conversion of a 3D object model to specific instructions for the printer"

[^1]:[slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing))

J'ai téléchargé l'[exécutable](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.176205037.954279970.1647372356-1735415410.1647372356) pour pouvoir installer le software.

### 4.3.2 Utilisation

Pour utiliser PrusaSlicer, il faut ouvrir dans le logiciel un fichier STL.

  Ensuite les différentes étapes sont reprises dans ce [manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md). Pour définir les paramètres de l'imprimante 3D en fonction du matériau choisi, voici le [guide](https://help.prusa3d.com/en/materials#_ga=2.244086342.219393710.1647608806-1375085091.1647608806).

Une fois que notre pièce est prête, on exporte le G-code, on l'upload sur une carte SD et on imprime notre fichier sur l'imprimante.

## 4.4 Projet de groupe

Nous avons décidé d'imprimer le [mini all in one 3D printer test](https://www.thingiverse.com/thing:2806295) comme *torture test* afin de pouvoir connaître les différentes particularité de l'imprimante 3D Prusa.

Nous avons téléchargé ce fichier STL et nous l'avons ouvert dans PrusaSlicer.

En suivant les [instructions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md), voici les paramètres que nous avons défini :

![couches et périmètres](../images/module4/couche.JPG)

Pour le remplissage, nous avons appris également que nous pouvions prendre 15% lorsqu'il s'agit d'une structure classique (déco) et 25% à 30% pour des structures plus solides.

Pour le motif, *Nid d'abeille* permet d'avoir des structures solides et *Grille* est utilisé pour des structures plus simples.

![remplissage](../images/module4/remplissage.JPG)

Pour minimiser la quantité de matériaux utilisés, il faut utiliser des supports et des bordures uniquement quand cela est nécessaire. Ici puisque nous voulons savoir qu'elles sont les caractéristiques de l'imprimante nous n'avons pas mis de supports.

![supports](../images/module4/supports.JPG)

Nous avons décidé de le faire en PLA.

![filament](../images/module4/filament.JPG)

Sur le plateau, nous pouvons définir l'échelle de notre structure. Afin d'avoir un temps d'impression pas trop long, et l'échelle de notre structure de test n'étant pas précise, nous sommes parti sur une échelle de 80% nous permettant d'avoir un temps d'impression de 1h50.

![plateau](../images/module4/Plateau.JPG)

Nous avons exportez le G-code et nous l'avons mis sur une des cartes SD du Fablab.

En suivant les [instructions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md) nous avons lancé l'impression.

![](../images/module4/test.jpg)

Maintenant que nous avons notre structure, nous pouvons analyser les différents tests se trouvant sur celle-ci (tout en gardant en tête que nous avons défini une échelle de 80% impactant les dimensions).
Le *hoverhang test* montre qu'à partir d'un angle de 60° la structure devient imparfaite.

|**Test** | **Analyse**
| :-- | :--
| *Overhang test* | Par pas de 10° : jusque 50° tout va bien, à partir d'un angle de 60° des défauts apparaissent, les couches ne sont plus nettes ni droites, elles ondulent. Par pas de 15° : jusque 45° tout va bien, à partir de 60° les couches ne sont plus nettes ni droites, elles ondulent et ont tendance à couler.
| *Support test* | Ok
| *Bridging test* | Tous les ponts ont tenu. Les quatre premiers sont solides (80% de 2mm, 5mm, 10mm et 15mm). Le pont de 80% de 20mm est un petit peu flexible à son centre. Le pont de 80% de 25mm est très flexible.
| *Scale test* | Ok
| *Stringing test* | Les tiges sont très fragiles.
| *Diameter test* | Ok
| *Hole test* | Les trous de 80% de 3mm, 4mm et 10mm sont ok. Par contre celui de 80% de 2mm ne ressemble pas à un cercle mais plutôt à un hexagone.

## 4.5 Projet personnel - Flexlinks kit

Il me faut un kit de flexlinks. J'ai décidé d'utiliser le flexlink que j'ai désigné pour le module 2 ainsi que deux autres flexlinks désignés par d'autres étudiants de cette année.

### 4.5.1 Flexlink 1

Mais d'abord je dois ajuster les dimensions des flexlinks pour qu'ils puissent s'adapter avec les lego. En me basant sur les [données de lego](https://www.tipsandbricks.co.uk/post/1418-tips-lego-dimensions-and-units), j'ai choisi les paramètres suivants pour mon flexlink:

```
radius_part = 3.4;
radius_hole = 2.5;
height = 3;
distance_cylinder = 8;
distance_part = 25;
```

Pour le rayon des trous, j'ai ajouté 0,1mm en plus des dimensions du lego pour pouvoir les emboîter. J'ai décidé de d'abord imprimer ma pièce pour vérifier que les dimensions sont correctes.

![](../images/module4/flexlinks.jpg)

Comme le montre la photo, les dimensions sont adaptées aux lego.

### 4.5.2 Flexlink 2

Pour mon deuxième Flexlink, j'ai décidé de prendre celui de Nathalie Wéron. Pour cela j'ai ajouté dans mon code "FROM : Nathalie Wéron, ovni_final.scad last modified on 24/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/blob/main/docs/FabZero-Modules/CAD/ovni_final.scad)" qui me permet de dire que ce code a été écrit part Nathalie (cela respecte sa creative commons license) et j'ai également ajouté la "DATE OF MODIFICATION" pour dire quand j'ai modifié son code pour avoir un suivi chronologique. J'ai adapté son code pour conserver les dimensions qui me conviennent pour les lego.

```
/*

FILE : flexlink_lego_2.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Nathalie Wéron, ovni_final.scad last modified on 24/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/blob/main/docs/FabZero-Modules/CAD/ovni_final.scad)

LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

*/

$fn = 50;
height = 3;
middle_length = 90;
middle_thickness = 1;
radius_part = 3.4;
radius_hole = 2.5;
center_holes_distance = 8;

h = height;
d = middle_length;
w = middle_thickness;
rc = radius_part;
e = center_holes_distance;
hole = radius_hole;

difference(){
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])cube([w,d,h]);
        
        //extremity 1
        cylinder(h, r=rc);
        translate([0,e,0])cylinder(h, r=rc);
        translate([-rc,0,0])cube([2*rc,e,h]);
        
        //extremity 2
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])cylinder(h, r=rc);
            translate([-rc,0,0])cube([2*rc,e,h]);
        }
        
        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])cylinder(h, r=rc);
                translate([-rc,0,0])cube([2*rc,3*e,h]);
            }
            
            //extremity1_body
            translate([0,-(e+e/2),0]){
                rotate([0,0,45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,-2*e,0])cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])cylinder(h,r=rc);
                }
            }
            
            //extremity2_body
            translate([0,e+e/2,0]){
                rotate([0,0,-45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,0,0])cube([2*rc,2*e,h]);
                    translate([0,2*e,0])cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES
    
    //extremity 1_holes
    translate([0,0,-1])cylinder(h+2, r=hole);
    translate([0,e,-1])cylinder(h+2, r=hole);
    
    //extremity 2_holes
    translate([0,d+(2*rc+e),0]){
        translate([0,0,-1])cylinder(h+2, r=hole);
        translate([0,e,-1])cylinder(h+2, r=hole);
    }
    
    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),-1]){ //put at centre
                cylinder(h+2, r=hole);
                translate([0,e,0])cylinder(h+2, r=hole);
                translate([0,2*e,0])cylinder(h+2, r=hole);
                translate([0,3*e,0])cylinder(h+2, r=hole);
                translate([-hole,e,0])cube([2*hole,e,h+2]);
            }
            
            //extremity1_holes
            translate([0,-(e+e/2),0]){//same as body
                rotate([0,0,45]){//same as body
                    translate([0,-e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,-2*e,-1])cylinder(h+2,r=hole);
                }
            }
         
            //extremity2_holes
            translate([0,e+e/2,0]){ //same as body
                rotate([0,0,-45]){ //same as body
                    translate([0,e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,2*e,-1])cylinder(h+2,r=hole);
                }
            }
        }
}
```
Voici à quoi ressemble la pièce :

![](../images/module4/flexlink2.JPG)

### 4.5.3 Flexlink 3

Pour mon troisième Flexlink, j'ai décidé de prendre celui de Alexandre Stoesser. Pour cela j'ai également spécifié "FROM" et "DATE DE MODIFICATION" dans mon code. J'ai adapté son code pour conserver les dimensions qui me conviennent pour les lego.

```
/*

FILE : flexlink_lego_3.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Alexandre Stoesser, Flexible3Dv2.scad last modified on 22/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/blob/main/docs/Files/Flexible3Dv2.scad)

LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

*/

$fn=50;

height = 3;
middle_length = 30;
center_holes_distance = 8;
structure_length = 18;
structure_width = 3.4*2;
diameter_hole = 2.5*2;

epaisseur = height;
longueur_de_la_tige =middle_length;
distance_entre_les_trous = center_holes_distance;
longueur_du_bloc = structure_length;
largeur_du_bloc = structure_width;
largeurtrou = diameter_hole;


largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;



width=1; depth = longueur_de_la_tige;


x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0; 

x1=0; y1=y0+distance_entre_les_trous/2; z1=0; 

x2=0; y2=y0-distance_entre_les_trous/2; z2=0; 

union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);
    
     
}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){
    
 

    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){
    
    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}

```
Voici à quoi ressemble la pièce :

![](../images/module4/flexlink3.JPG)

### 4.5.4 kit

Pour éviter d'imprimer les deux pièces séparemment j'ai créé un fichier OpenSCAD avec les deux flexlinks et je les imprimerai en une fois.

```
/*

File : Flexlinks_lego_2_3.scad

Author : Pauline Mackelbert

Date : 30 mars 2022

License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

*/

//parameters
radius_part = 3.4;
radius_hole = 2.5;
height = 3;
distance_cylinder = 8;
distance_part = 25;

$fn=100;

/* 
Flexlink_lego_2

FILE : flexlink_lego_2.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Nathalie Wéron, ovni_final.scad last modified on 24/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/blob/main/docs/FabZero-Modules/CAD/ovni_final.scad)
*/

middle_length = 90;
middle_thickness = 1;

h = height;
d = middle_length;
w = middle_thickness;
rc = radius_part;
e = distance_cylinder;
hole = radius_hole;

translate([distance_cylinder*3,0,0])
difference(){
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])cube([w,d,h]);
        
        //extremity 1
        cylinder(h, r=rc);
        translate([0,e,0])cylinder(h, r=rc);
        translate([-rc,0,0])cube([2*rc,e,h]);
        
        //extremity 2
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])cylinder(h, r=rc);
            translate([-rc,0,0])cube([2*rc,e,h]);
        }
        
        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])cylinder(h, r=rc);
                translate([-rc,0,0])cube([2*rc,3*e,h]);
            }
            
            //extremity1_body
            translate([0,-(e+e/2),0]){
                rotate([0,0,45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,-2*e,0])cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])cylinder(h,r=rc);
                }
            }
            
            //extremity2_body
            translate([0,e+e/2,0]){
                rotate([0,0,-45]){
                    //fix the futur centre at the origin before the rotation
                    translate([-rc,0,0])cube([2*rc,2*e,h]);
                    translate([0,2*e,0])cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES
    
    //extremity 1_holes
    translate([0,0,-1])cylinder(h+2, r=hole);
    translate([0,e,-1])cylinder(h+2, r=hole);
    
    //extremity 2_holes
    translate([0,d+(2*rc+e),0]){
        translate([0,0,-1])cylinder(h+2, r=hole);
        translate([0,e,-1])cylinder(h+2, r=hole);
    }
    
    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),-1]){ //put at centre
                cylinder(h+2, r=hole);
                translate([0,e,0])cylinder(h+2, r=hole);
                translate([0,2*e,0])cylinder(h+2, r=hole);
                translate([0,3*e,0])cylinder(h+2, r=hole);
                translate([-hole,e,0])cube([2*hole,e,h+2]);
            }
            
            //extremity1_holes
            translate([0,-(e+e/2),0]){//same as body
                rotate([0,0,45]){//same as body
                    translate([0,-e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,-2*e,-1])cylinder(h+2,r=hole);
                }
            }
         
            //extremity2_holes
            translate([0,e+e/2,0]){ //same as body
                rotate([0,0,-45]){ //same as body
                    translate([0,e,-1])cylinder(h+2,r=hole);//same holes cylinders, h+2, r=hole
                    translate([0,2*e,-1])cylinder(h+2,r=hole);
                }
            }
        }
}

/*
Flexlink_lego_3

FILE : flexlink_lego_3.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Alexandre Stoesser, Flexible3Dv2.scad last modified on 22/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/blob/main/docs/Files/Flexible3Dv2.scad)
*/

middle_length2 = 30;
structure_length = 18;

epaisseur = height;
longueur_de_la_tige =middle_length2;
distance_entre_les_trous = distance_cylinder;
longueur_du_bloc = structure_length;
largeur_du_bloc = radius_part*2;
largeurtrou = radius_hole*2;

largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;

width=1; depth = longueur_de_la_tige;

x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0; 
x1=0; y1=y0+distance_entre_les_trous/2; z1=0; 
x2=0; y2=y0-distance_entre_les_trous/2; z2=0; 

translate([distance_cylinder*8 ,middle_length2,0])
union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);   
}

module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){
    
    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){
    
    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}
```

Voici à quoi cela ressemble :

![](../images/module4/flexlinks_kit_plateau.JPG)

J'ai également fait un fichier OpenSCAD nommé "flexlinks_lego_kit" contenant les trois flexlinks et il se trouve sur le git.

En voulant imprimer les pièces la première fois j'ai eu quelques soucis. Cela commençait par imprimer la pièce de droite, puis en voulant passer à la pièce de gauche le filament s'entortillait. J'ai découvert que mes deux pièces n'étaient pas sur le même plan, et que l'imprimante voulait par conséquent imprimer la pièce de gauche en l'air...

![](../images/module4/Error.JPG)

Voici le kit au complet :

![](../images/module4/kit_complet.jpg)

### 4.5.5 *Compliant mechanism*

Il me reste maintenant à réaliser un *compliant mechanism* à partir de mon flexlink et de lego.

J'ai décidé de faire un méchanisme permettant de déplacer deux barres parallèlement.

<video controls>
 <source src="../../images/module4/Compliant_mechanism.mp4" type="video/mp4">
</video>

## 4.6 Checklist du module

- Explained what you learned from testing the 3D printers

- Documented how you used other people's work and gave proper credit to complete your kit

- Shown how you made your kit with words/images/screenshots

- Included your original design files for 3D printing (both CAD and generic (STL, OBJ or 3MF) files)

- Included a video (compressed <10 Mo) of your working mechanism
