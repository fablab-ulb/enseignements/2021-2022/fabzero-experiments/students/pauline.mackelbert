# 6. Electronique 2 - Fabrication

L'arduino UNO est très facile d'utilisation et une grande communauté l'utilise mais il est assez vieux et cher. Nous allons donc apprendre cette semaine comment fabriquer notre propre carte arduino.

## 6.1 Comment fabriquer son propre arduino

Nous allons nous baser sur un microprocesseur [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) pour créer notre carte Arduino.

Il est plus compliqué que celui de l'Arduino UNO mais il est bien plus puissant. Il existe de bonnes librairies qui permettent de le manipuler facilement (sinon il est possible de coder en C mais il est alors nécessaire de configurer tout ce qui est rendu possible par ces librairies).

## 6.2 Fabrication étape par étape

Les différentes étapes suivent ce [manuel](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md).

Voici la liste du matériel nécessaire :

* carte pré-usinée
* microcontrôleur [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14)
* régulateur de tension (pour changer les 5V en 3,3V)
* 2 capacité 3,3V
* résistance 1001 Ohm
* 2 résistances 3300 Ohm
* 2 LEDs
* 2 rangées de 6 pins

Nous avons reçu au départ un carte pré-usinée, il s'agit d'un matériau recouvert d'une fine couche de cuivre usiné précisément pour enlever le cuivre afin de tracer le circuit électrique.

![](../images/module6/2_3_accroche_carte_vide.jpg)

Nous allons la coller à la table pour éviter qu'elle ne bouge pendant que l'on soude.

La première chose à faire est de régler la température de notre fer à souder : elle doit être supérieure à 300° et plus elle est chaude, plus il est facile de souder, mais attention à ne pas surchauffer car cela peut affecter la puce, on nous a conseillé de commencer avec 330°.

![](..images/../../images/module6/1matos.jpg)

### 6.2.1 Souder le microcontrôleur

Il faut faire attention à bien l'orienter (le point doit être à gauche en haut quand on oriente le connecteur USB de la carte vers le bas). Il faut commencer par souder un pin à l'un des coins, puis le pin opposé pour qu'il soit fixé.

Il faut faire attention à ne pas rester trop longtemps sur les pins pour éviter de surchauffer le microcontrôleur.

![](../images/module6/4_5_points.jpg)

J'ai eu du mal à faire des petits points de soudure et je créais des ponts entre les pins. J'ai réussi à les séparer en utilisant une pompe à déssouder qui aspire le métal liquide. J'ai également changé de pointe à souder.

![](../images/module6/6_7_pointes.jpg)

Je suis passée d'une pointe fine à une pointe plus large. En effet, il était difficile d'avoir un transfert de chaleur suffisant pour faire fondre le fil d'étain.

Après de nombreuses tentatives, voici le résultat :

![](../images/module6/8processor.jpg)

### 6.2.2 *Bootloader*

Une fois avoir soudé le régulateur, la capacité, deux résistances, une LED et les deux rangées de pins...

![](../images/module6/9_10_11_12_elements.jpg)

... nous connectons notre carte à l'ordinateur afin de d'uploader le *bootloader*. 

![](../images/module6/13test1.jpg)

### 6.2.3 Souder les derniers éléments

Finalement, nous pouvons souder tous les derniers éléments, voici le résultat : 

![](../images/module6/14carte_finale.jpg)



Pour vérifier que toute les soudures étaient correctement faites, et comme il n'est pas facile de voir à l'oeil nu s'il y a un réel contact entre la carte et la pin, nous avons utilisé un multimètre pour voir si le courant passait entre certains pins.

## 6.3 Utilisation

En connectant ma carte au port USB de mon ordinateur , celui-ci ne le reconnaissait pas... Malheureusement nous avons été plusieurs dans le groupe à avoir eu le cas.

Nous avons vérifier que toutes les soudures étaient correctement faites : comme il n'est pas facile de voir à l'oeil nu s'il y a un réel contact entre la carte et la pin, nous avons utilisé un multimètre pour voir si le courant passait entre certaines pins spécifiques. Mais tout semblait fonctionner correctement. Après plusieurs heures à chercher quel pouvait être le problème avec les autres étudiants du groupe ayant le même problème, nous n'avons pas trouvé de solution.

Si celle ci fonctionnait, j'aurais dû écrire un petit code et le faire fonctionner sur notre carte (les explications se trouvent [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md#settings)).