# 2. Conception Assistée par Ordinateur

La tâche de cette semaine est de désigner et modéliser un kit de construction paramétrique [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks).

## 2.1 Les mécanismes flexible et les flexions

*Compliant mechanism* [^1]
: "flexible mechanism that achieves force and motion transmission through elastic body deformation. It gains some or all of its motion from the relative flexibility of its members rather than from rigid-body joints alone."

[^1]: [compliant mechanism](https://en.m.wikipedia.org/wiki/Compliant_mechanism)

En fonction des contraintes s'appliquant sur la structure et les différents degrés de liberté, certaines déformations sont possibles (translations et rotations).
Il est également possible de déformer une pièce si le matériau n'est pas rigide ou si la géométrie de la structure le permet.

*Bistable mechanism* [^2]
: "mechanism that has two stable equilibrium positions within their range of motion. Its advantages include the ability to stay in two positions without power input and despite small external disturbances."

[^2]: [Bistable mechanism](https://www.researchgate.net/publication/259291073_Compliant_bistable_mechanisms#:~:text=Bistable%20mechanisms%20are%20mechanisms%20that,and%20despite%20small%20external%20disturbances.)

Ces différents méchanismes peuvent être imprimés en 3D. Mais tout d'abord il faut concevoir ces structures sur un programme spécialisé.

## 2.2 Logiciel 3D

Il existe de nombreux logiciels 3D et ils se divisent en deux grandes catégories, en fonction qu'ils traitent des images matricielles (pixels) ou des images vectorielles.

La première méthode est de travailler sur une image au niveau de ses pixels, nous pouvons modifier le contraste, appliquer un filtre, etc. Pour ce faire, nous pouvons utiliser les programmes suivants :

- Paint
- GIMP
- Blender : inclut un module pour l'animation physique 
  
Contrairement aux images matricielles, les images vectorielles ne sont pas composées de pixels mais d'objets géometriques individuels (droites, polygones, arcs de cerle)[^3] et un code décrit ces différents objets et comment ils sont connectés. Il s'agit donc de réelle description des différents systèmes. Nous pouvons utiliser les programmes suivants :

- Inkscape
- AutoCAD
- Solidworks
- Maya : pour les vidéos
- Antimony : au lieu d'écrire complètement le code, il est possible de travailler avec des blocs (chacun représentant un partie de code) en les connectant entre eux.

[^3]: [image vectorielle](https://www.macapflag.com/blog/image-vectorielle/)

Les avantages des images vectorielles sont les suivants :

- elles peuvent être agrandie à l'infinie
- elles sont plus légères que les images matricielles

Un avantage des logiciels décrivant la géométrie des pièces : ils permettent de changer un facteur de la pièce et celle-ci se modifiera en conséquence.

Pour pouvoir imprimer une pièce en 3D, l'imprimante doit recevoir un chemin décrivant la structure. Pour ce cela, il faut utiliser un logiciel 3D travaillant sur les images vectorielles. Nous avons découvert [OpenSCAD](https://openscad.org/) et [FreeCAD](https://www.freecadweb.org/).

### 2.2.1 OpenSCAD

Ce logiciel fonctionne en decrivant la géométrie d'une pièce en écrivant du code. Pour ce faire il faut ouvrir OpenSCAD, commencer un nouveau fichier et écrire dans l'éditeur.

Il existe une [cheat sheet](https://openscad.org/cheatsheet/) dans laquelle se trouvent toutes les fonctions utiles à connaître pour démarrer un projet. Il y a des fonctions qui permettent d'écrire le code (des opérateurs, des constantes, des variables, ...) et puis il y a des fonctions permettant de décrire les formes géométriques.

#### 2.2.1.1 Formes géométriques

Tout d'abord voici les formes géométriques de base :

| 2D | 3D | Arguments
| :-- | :-- | :--
| `circle` | `sphere` | *radius* ou d=*diameter*
| `square` | `cube` | *size*, *center*
| | | [*width*,(*depth*,) *height*], *center*
| `polygon` | | [*points*] ou [*points*], [*paths*]
| | `cylinder` | *height*, *radius* or *diameter*, *center*
| | `polyhedron` | *points*, *faces*, *convexity*

Voici un exemple dans OpenSCAD : ![ ](../images/module2/Openscad_rendu.jpg)

Nous pouvons également voir sur quel bouton il faut pousser pour voir l'aperçu de la structure.

Voici un autre exemple : `cylinder(r=50,h=10);`

![ ](../images/module2/cylindre.JPG)

Par contre, il semblerait que l'image soit pixelisée, pour éviter ce rendu, il est possible d'augmenter le nombre de fragments à utiliser, par contre il faut garder en tête que plus ce nombre sera grand, plus lente sera l'exécution du code.

```
$fn=500;
cylinder(r=50,h=10);
```

![ ](../images/module2/cylindre_smooth.JPG)

#### 2.2.1.2 Opérations booléennes

Il est également possible de combiner certaines formes, cela se fait avec des opérations booléennes :

- `union()`
- `difference()`
- `intersection()`
- `hull()` : défini une surface couvrant tous les coins externes des formes mises en argument
- `minkowski()` : somme des sommets

```
$fn=100;
side= 10;
union(){
  cube(side,center=true);
  sphere(r=side/sqrt(2));
}
```

![ ](../images/module2/union.JPG)


```
$fn=100;
side= 10;
hull(){
  cube(side,center=true);
  sphere(r=side/sqrt(2));
}
```

![ ](../images/module2/hull.JPG)

#### 2.2.1.3 Transformations

Ensuite, voici les transformations possibles sur ces formes :

| Transformation | Arguments
| :-- | :--
| `translate` | [x,y,z]
| `rotate` | [x,y,z] ou a, [x,y,z]
| `scale` | [x,y,z]
| `resize`| [x,y,z], *auto*, *convexity*
| `mirror`| [x,y,z]

Il est possible de faire plusieurs transformations à la suite sur la même pièce, il suffit de mettre ";" lorsque nous avons fini notre séquence de transformations :

```
rotate([45,0,0])
rotate([0,45,0])
translate([0,0,10])
cube(10);
```

![ ](../images/module2/transformations.JPG)

Il est également possible d'animer ces transformations :

```
rotate([0,0,$t*360])
translate([20,0,0])
cube(10);
```
Ensuite dans Vue>Animer> il est possible de choisir Temps, FPS et Étapes.

#### 2.2.1.4 Remarques

Il faut savoir qu'il n'y a pas d'unité précisée mais nous pouvons considérer qu'il s'agit de millimètres. Lorsque nous allons vouloir imprimer la pièce en 3D, il faudra importer notre fichier et préciser les unités.

Il est important de coder de manière paramétrique. C'est à dire qu'au lieu de mettre directement les valeurs souhaitées dans le code, nous définissons des paramètres et utilisons ceux-ci dans le code. Cela permet de pouvoir facilement changer les valeurs de certains paramètres sans devoir récrire tout le code.

Il existe une [bibliothèque d'objets](www.thingiverse.com) comprenant une multitude de projets à pouvoir refaire soi-même.

Il existé également un librairie contenant plein de codes pour des formes géométriques plus complexes. Il s'agit de [BOSL](https://github.com/revarbat/BOSL/wiki/shapes.scad).

### 2.2.2 FreeCAD

Ce logiciel ressemble plus à Solidworks, il y a moins de code et plus de modules.

Le principe est de définir une esquisse 2D et puis nous pouvons passer en 3D et agir sur la pièce.

En regardant le [tutoriel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/FreeCAD.md), je me suis lancée le défi de refaire la même chose mais cette fois-ci en créant un hexagone.

La première étape consiste à définir une esquisse 2D de notre forme dans *Part Design*.

![ ](../images/module2/sketch.JPG)

Pour le moment aucune contrainte n'a encore été définie, il y a donc un grand nombre de degrés de liberté.

J'ai d'abord lié les différentes lignes ensemble en connectant les points correspondant entre eux. Ces contraintes de coïncidence on pu réduire le nombre de degrés de liberté (ddl) à 11.
Ensuite j'ai mis des contraintes parallèles sur les différentes arrêtes. Cela a réduit le nombre de ddl à 8. Avec des contraintes de symétrie et en fixant certains points sur les axes, il ne reste plus que 3 ddl. Finalement en fixant la longueur de deux côtés à 20mm et un angle intérieur à 120° nous obtenons un hexagone n'ayant plu de ddl.

![ ](../images/module2/hexagone.JPG)

Finalement, en suivant les instructions du tutoriel, j'obtiens la pièce suivante :

![ ](../images/module2/hexa.JPG)

### 2.2.3 Comparaison de OpenSCAD et FreeCAD

L'avantage de OpenSCAD est qu'il s'agit de textes et qu'avec le temps ces textes seront toujours utilisables alors que certains fichiers de FreeCAD pourraient devenir incompatibles avec de nouvelles versions du logiciel.

L'avantage de FreeCAD est qu'il y a bien plus de choses que nous pouvons faire.

Par contre, il est possible de décrire la pièce avec OpenSCAD et puis d'ouvrir celle-ci dans FreeCAD pour la modifier avec les outils disponibles uniquement sur FreeCAD (exemple : les bordures).

Pour imprimer une pièce définie sur OpenSCAD ou FreeCAD, il faut exporter la pièce en format .stl

Format STL [^4]
: "Un format STL représente la géométrie de la surface 'un objet en trois dimensions".

[^4]: [STL](https://www.freelabster.com/fr/blog/quest-quun-fichier-stl/)

## 2.3 FlexLinks

Tout d'abord, j'ai défini les paramètres :

```
//parameters
radius_part = 2;
radius_hole = 1;
height = 6;
distance_cylinder = 4;
distance_part = 10;
```

Ensuite j'ai créé des modules de plus en plus complet et dépendant des précédents pour construire ma pièce. Voici les étapes :

```
doublecylinder(height,radius_part,distance_cylinder);

module doublecylinder(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    //create two cylinders
    union(){
        cylinder(h=height,r=radius,center=true);
        translate([distance,0,0])cylinder(h=height,r=radius,center=true);
    }
}
```

![ ](../images/module2/doublecylinder.JPG)

```
largecylinder(height,radius_part,distance_cylinder);

module largecylinder(height,radius,distance_cylinder)
{
    //hull the two cylinders
    hull(){
        doublecylinder(height,radius,distance_cylinder);
    }      
}
```

![ ](../images/module2/largecylinder.JPG)

```
part(height,radius_part,radius_hole,distance_cylinder);

module part(height,radius_part,radius_hole,distance_cylinder)
{
    //remove the two cylinders to have holes
    difference(){
        largecylinder(height,radius_part,distance_cylinder);
        doublecylinder(height,radius_hole,distance_cylinder);
    }
}
```

![ ](../images/module2/part.JPG)

```
doublepart(height,radius_part,radius_hole,distance_cylinder,distance_part);

module doublepart(height,radius_part,radius_hole,distance_cylinder,distance_part)
{
    //create two parts
    union(){
        part(height,radius_part,radius_hole,distance_cylinder);
        translate([0,distance_part,0])part(height,radius_part,radius_hole,distance_cylinder);
    }
}
```

![ ](../images/module2/doublepart.JPG)

```
arc(distance_part,height,radius_part,radius_hole);

module arc(distance_part,height,radius_part,radius_hole)
{
    difference(){
        difference(){
            translate([-radius_part,distance_part/2,0])cylinder(h=height,d=distance_part,center=true);
            translate([-radius_part,distance_part/2,0])cylinder(h=height,d=distance_part-1,center=true);
        }
    translate([-radius_part+distance_part/4,distance_part/2,0])cube([distance_part/2,distance_part,height],center=true);    
    }
}
```

![ ](../images/module2/arc.JPG)

```
flexlinks(height,radius_part,radius_hole,distance_cylinder,distance_part);

module flexlinks(height,radius_part,radius_hole,distance_cylinder,distance_part)
{
    union(){
        doublepart(height,radius_part,radius_hole,distance_cylinder,distance_part);
        arc(distance_part,height,radius_part,radius_hole);
    }
} 
```

![ ](../images/module2/flexlinks.JPG)

Finalement pour vérifier que cela fonctionne bien, j'ai essayé d'autres paramètres :

```
//parameters
radius_part = 3;
radius_hole = 2;
height = 8;
distance_cylinder = 6;
distance_part = 25;
```

![ ](../images/module2/flexlinks2.JPG)

## 2.4 Licenses

*CC Licenses* [^5]
: *Creative Commons licenses give everyone from individual creators to large institutions a standardized way to grant the public permission to use their creative work under copyright law.*

[^5]: [CC Licenses](https://creativecommons.org/about/cclicenses/)

Mettre une *creative license* sur notre travail permet aux autres d'utiliser notre travail. Ce qui est tout l'intérêt de cette documentation.

Il en existe plusieurs options.

| Options | Significations 
|:-- | :--
| *BY* | *Credit must be given to the creator*
| *SA* | *Adaptations must be shared under the same terms*
| *NC*| *Only noncommercial uses of the work are permitted*
| *ND* | *No derivatives or adaptations of the work are permitted*

Il existe donc différents types de licenses : 

- CC BY
- CC BY-SA
- CC BY-NC
- CC BY-NC-SA
- CC BY-ND
- CC BY-NC-ND

Pour *CC-licensing* nos codes, il suffit de choisir le type de license que l'on veut et communiquer clairement cette license dans notre travail (il faut inclure le lien de la license).

Voici comment j'ai ajouté ma license sur mon fichier OpenSCAD.
```
// File : Flexlinks_module2.scad

//Author : Pauline Mackelbert

// Date : 16 mars 2022

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```
## 2.5 Checklist du module 

- parametrically modelled some FlexLinks using 3D CAD software

- Shown how you did it with workds/images screenshots

- included your original design files

- included the licence you chose

## 2.6 Abbréviations

STL
: STéréoLithographie 