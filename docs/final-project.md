# Projet final

Dans le premier chapitre je vais définir mon idée de départ. Ensuite dans le second je vais réaliser le *challenge-based learning framework* afin de définir ma problématique. Le troisième concerne les SDG des nations unies. Dans le quatrième chapitre je développe la solution. Dans le cinquième je résume la fabrication du prototype avec le matériel, le prix et le temps de fabrication. Le sixième concerne les pistes d'amélioration. Pour finir par une conclusion.

## 1. Idées de départ

Lors de mon stage chez [Eat's Local](https://www.bwbx.eatslocal.be/) j'ai vu combien de colis entraient dans l'entrepôt pourtant il s'agissait d'un entrepôt de 100m². Comme ils livrent les clients dans des bacs réutilisables, ces caisses en carton n'avaient plus d'utilité et elles étaient jetées. Je suis sûre que ce problème doit arrivé dans la majorité des entreprises se faisant livrer leurs produits.

La problématique observée est donc **comment faire pour donner une utilité à ces caisses en carton**.

## 2. *Challenge-based learning framework*

*Challenge Learning Framework*
 : "*is divided into three interconnected phases: Engage, Investigate and Act. Each phase includes activities that prepare you to move to the next phase. Within each of the phases there are opportunities for mini-investigation cycles and if necessary a return to an earlier phase. Supporting the entire process is an ongoing process of documentation, reflection and sharing*" [^1]

[^1]: [Challenge Learning Framework](https://www.challengebasedlearning.org/framework/)

Pour trouver ma problématique, je vais utiliser le *challenge learning framework*.

### 2.1 ***Engage* - trouve un défi qui te motive**

#### A. *Big ideas*: quel est le thème ou le concept général que je souhaite explorer ?

J'aimerais pouvoir utiliser les principes de l'économie circulaire et des low-techs pour résoudre certains problèmes de gaspillage de la vie de tous les jours.

Voici les principes de l'économie circulaire :

![](../images/project/économie_circulaire.jpg) [^2]

[^2]: [Be circular brussels](https://www.circulareconomy.brussels/a-propos/leconomie-circulaire/)

La low-tech
: "est un ensemble de technologies utiles, durables et accessibles à tous. En effet, la low-tech s'appuie sur ces 3 piliers. Par son utilité, elle souhaite apporter des savoir-faire et des technologies pour permettre un meilleur accès à la santé, l'alimentation ou encore l'eau potable. Les technologies Low-tech doivent être accessibles à tous pour permettre à chacun d'être le changement de demain". [^3]

Un des cas de gaspillage qui me marque le plus est la quantité de carton utilisée lors des livraisons.

[^3]: [Les low-techs](https://www.futura-sciences.com/tech/definitions/low-tech-low-tech-18829/)

#### B. *Essential questions*: quelles sont les questions essentielles qui reflètent les intérêts personnels et les besoins de la communauté ?

Peut-on utiliser les principes de l'économie circulaire et des low-techs pour résoudre des problèmes de gaspillage de tous les jours ?

Comment faire pour développer une solution dans un Fablab ?

#### C. *Challenge*: quel est votre appel à l'action ?

En considérant d'abord *l'offre des acteurs économiques* de l'économie circulaire, je pourrais m'intéresser à la fabrication des caisses en carton, à comment certains entrepreneurs innovent sur les méthodes de livraison ou sur le recyclage des cartons.
Par rapport à *la demande, le comportement et la gestion des besoins des consommateurs*, il serait possible de sensibiliser les gens sur l'importance de recycler son carton, peut être sonder les clients sur l'utilisation de caisses en carton.
Finalement *la gestion des ressources et des déchets* me permet de m'interroger sur la méthode actuelle de recyclage du carton et également sur des concepts existants de réutilisation du carton.

Je pense que ce qui pourrait le plus mêler l'économie circulaire et les low-techs serait de m'intéresser aux méthodes de recyclage du carton, aux valorisations innovantes du carton dans certaines entreprises et de voir s'il y a moyen de rendre ces méthodes plus accessibles au grand public.

### 2.2 ***Investigate* - marche sur les épaules des géants, pas sur leurs orteils**

#### A. *Guiding questions*: quelles sont les questions auxquelles il faut répondre pour relever le défi ? Quelle est la priorité ?

* Est-ce que le carton peut réellement se recycler à l'infini ?

* Est-ce que les caisses en carton causent une partie de la déforestation ?

* Peut-on faire quelque chose d'utile des boîtes en cartons usagées dans un entrepôt ?

* Peut-on rendre ces méthodes de valorisation plus accessibles ?
  
* Quelles sont les entreprises valorisant leurs caisses en carton ?

#### B. *Guiding activities/resources*: quelles ressources puis-je utiliser pour répondre à ces questions ? Science

* [Bruxelles-propreté](https://www.arp-gan.be/fr/traitement.html)
* [Recy-K](https://www.arp-gan.be/fr/Recy-K.html) : "plateforme de l'économie circulaire et de l'économie sociale, spiécialisée dans la réutilisation, la réparation, le réemploi et le recyclage de déchets/ressources" située à Bruxelles
* [Recyclis](https://www.recyclis.be/environnement.html) : traite le trie et le recyclage des papiers-cartons
* [Fost Plus](https://www.fostplus.be/fr/a-propos-de-Fost-Plus) : le moteur du recyclage et de l'économie circulaire de l'emballage ménager en Belgique

#### C. *Analysis and synthesis*: rédiger un résumé de vos conclusions, des faits et des données receuillies

Le carton est l'une des matières les plus utilisées dans l'industrie de l'emballage et donc dans la logistique. Pour fabriquer les boîtes en carton nous utilisons généralement du carton ondulé, mais **qu'est ce que c'est ?**

Papier cannelé [^4]
 : papier ayant subi un traitement destiné à lui conférer des ondulations régulières et permanentes

Carton ondulé [^4]
 : carton consistant en une ou plusieurs feuille(s) de papier cannelé collées sur une feuille de carton ou entre plusieurs feuilles de carton

![](../images/project/Carton_ondulé_.jpg) [^5]

[^5]: [Le carton ondulé](https://www.universepack.com.tn/les-3-fondamentaux-a-connaitre-sur-le-carton-ondule/)

Carton ondulé simple cannelure / carton ondulé double face [^4]
 : carton consistant en une feuille de papier cannelé intercalée entre et collée à deux surfaces

![](../images/project/simple_cannelure_tranche.jpg) [^6]

[^6]: [Image carton simple cannelure](https://www.embalpro.com/nos-produits/caisses-carton-caisses-bois/caisses-americaines/)

[^4]: [ISO carton/papier](https://www.iso.org/obp/ui#iso:std:iso:4046:-4:ed-2:v1:fr:sec:4.166)

Sur l'image qui suit nous pouvons voir le procédé de fabrication de ce carton ondulé.

![](../images/project/schema-onduleuse.jpg) [^7]

[^7]: [Carton ondulé de France](https://www.cartononduledefrance.org/materiau-et-innovation/materiau-carton/#:~:text=L'emballage%20en%20carton%20ondul%C3%A9,normes%20d'hygi%C3%A8ne%20en%20vigueur%20%3A&text=Il%20est%20tout%20d'abord,utilisation%20r%C3%A9p%C3%A9t%C3%A9e%20du%20m%C3%AAme%20emballage.)

##### a. Pourquoi utilise t'on du carton ondulé ?

Les feuilles de carton "participent à la résistance mécanique et climatique de l’emballage et servent de support de communication et/ou d’information. Les cannelures assurent la rigidité de l’emballage mais aussi une élasticité maximale puisqu’elles servent d’amortisseurs en cas de chocs". [^8]

[^8]: [Carton ondulé de France](https://www.cartononduledefrance.org/materiau-et-innovation/materiau-carton/#:~:text=L'emballage%20en%20carton%20ondul%C3%A9,normes%20d'hygi%C3%A8ne%20en%20vigueur%20%3A&text=Il%20est%20tout%20d'abord,utilisation%20r%C3%A9p%C3%A9t%C3%A9e%20du%20m%C3%AAme%20emballage.)

##### b. Est ce vraiment une bonne idée d'utiliser du carton ?

"Alors que la filière papetière contribue encore dans certaines régions à la déforestation et la dégradation des forêts, la situation européenne est aussi un bel exemple d’une économie circulaire possible. De grandes perspectives apparaissent pour cette partie de la filière papier, même si elle ne peut pas remplacer la production de papier fibres vierges. La faible part des achats de papier recyclé en France pénalise le développement de l’économie circulaire locale, avec tous ses atouts de réduction d’impacts environnementaux (eau, énergie, ressources) et de création d’emplois. Alors que les emballages et le papier journal contiennent un fort pourcentage de fibres recyclées, les papiers d’impression-écriture eux n’en contiennent en moyenne que 10% (Ecofolio, 2015). Il reste donc du progrès à faire, que ce soit dans le choix du recyclé ou dans le recyclage. [...] Pour choisir un papier responsable, il faut privilégier à la fois les écolabels qui valident les démarches de protection de l’eau et de l’air (Ange bleu, Ecolabel européen, Cygne nordique) ainsi que les labels garantissant l’utilisation de fibres recyclées ou de fibres issus de bois de forêts gérées durablement, comme la certification FSC. [...] La durabilité se traduit aussi par des choix d'éco conception, comme un nombre de tirages et de pages bien pensés, ou encore la création de produits plus facilement recyclable. Le recyclage est une part importante de la durabilité de la filière." [^9]

[^9]: [WWF : papier](https://www.wwf.fr/champs-daction/foret/approvisionnement-responsable/papier)

Le carton est fabriqué à partir de pulpe de bois préssée, séchée et puis compressée. Le bilan carbone de ce processus est d'environ 950 kilos par tonne d'équivalent en CO2 (le bilan carbone est calculé en fonction de la consommation en eau nécessaire pour fabriquer l'emballage et les émissions de gaz à effets de serre que ces emballages engendrent) [^10].
Pour réduire ce coût, nous pouvons nous orienter vers des arbres issus de forêts durables, dans lesquelles des arbres sont plantés pour chaque arbre coupé. Le cycle de vie du carton est bon puisque 80% des cartons sont recyclés.

Il est également possible de fabriquer la pâte à papier à partir de coupes d'éclaircies lors de l'entretien des forêts ou des chutes de bois produites par les scieries. [^11]

[^11]: [Le papier est-il mauvais pour l'environnement ?](https://ecotree.green/blog/le-papier-est-il-mauvais-pour-l-environnement)

Le carton ondulé semble très bien se défendre, il serait un des emballages les plus durables puisqu'"il est consistué de matières naturelles exclusivement, dont 80% de papier recyclé et qu'il est 100% renouvelable, 100% recyclable et 100% biodégradable [...]. Son impact carbone est très faible, estimé à 784 kilos de CO2 pour une tonne de carton." [^10]

"On l’a lu précédemment, utiliser du bois issus de forêts durables améliore le bilan carbone d’un emballage en carton. Ces forêts participent à la retenue du CO2 de manière efficace. L’industrie du carton en Europe participe d’ailleurs à ces opérations de reboisement, à hauteur de 17 millions d’hectares de forêt qui sont en gestion durable. Dans ce cas de figure, il pousse 33% d’arbres en plus que l’on en abat.
Mais c’est finalement en réduisant nos emballages, voire en les supprimant qu’on évite un rejet de 90 kilos d’équivalent de CO2 par an et par personne." [^10]

[^10]: [Empreinte carbone du carton](https://www.greenly.earth/blog-fr/empreinte-carbone-carton)

##### c. Comment sont gérés les "déchets" de carton?

**Valorisation** [^12]
: la valorisation des déchets désigne l'ensemble des opérations effectuées sur un objet inutile afin de le rendre à nouveau utile

**Préparation en vue du réemploi** [^12]
: toute opération de contrôle, de nettoyage ou de réparation en vue de la valorisation, par laquelle des produits ou des composants de produits qui sont devenus des déchets sont préprarés pour pouvoir être réutilisés sans autre opération de prétraitement

**Recyclage** [^12]
: toute opération de valorisation par laquelle les déchets sont retraités en produits, matière ou substances aux fins de leur fonction initiale ou à d'autres fins. Cela inclut le retraitement des matières organiques (mais pas la valorisation énergétique), la conversion pour l'utilisation comme combustible ou pour des opérations de remblayage

**Elimination** [^12]
: toute opération qui n'est pas de la valorisation même lorsque ladite opération a comme objectif secondaire la récupération de substances ou d'énergie

[^12]: Rapport annuel Bruxelles-Propreté 2020

Bruxelles-Propreté s'occupe entre autres de :

- la **prévention** en information la population
- la **réutilisation** via [Recy-K](https://www.arp-gan.be/fr/Recy-K.htm)
- le **tri et le recyclage** via [Recyclis](https://www.recyclis.be/environnement.html) pour le contenu des sacs bleu et jaunes
- l'**incinération avec récupération d'énergie** via Bruxelles-Energie

Concernant le tri et le recyclage, tous les emballages ménagers sont pris en charge par [Fost Plus](https://www.fostplus.be/fr/a-propos-de-Fost-Plus) et les emballages industriels par [ValIPac](https://www.valipac.be/).

"Aujourd'hui, nous évoluons clairement vers un modèle circulaire, où les matériaux sont préservés dans la chaîne en tant que matières premières secondaires. L'économie circulaire entend garder précieusement tout ce qui a de la valeur. L'usage de matériaux vierges ou de nouvelles matières premières primaires est ainsi limité autant que possible. En d'autres termes, lorsque les emballages sont nécessaires et utiles, il est important d'en collecter le plus possible en vue d'un recyclage. [...] En outre, dans la plupart des cas, la consommation d'énergie pour le recyclage est sensiblement moindre que pour la production de nouveaux matériaux." (Fost Plus) [^13]

[^13]: Dossier de presse Fost Plus 2022

Mais ce n'est pas tout, il existe également un mécanisme appelé Responsabilité Élargie du producteur (REP) qui a été "mis en place en vue de réduire les coûts de gestion des déchets supportés par les pouvoirs publics, d'augmenter les taux de réemploi, recyclage et valorisation [...]. Il peut également induire un effet de prévention en incitant les producteurs à modifier la conception de leurs produits afin d'en améliorer l'éco-efficience (production avec moins de ressources ou des matières recyclées) et l'écoconception (démontage et recyclage plus faciles, moins de substances dangereuses). Actuellement, le mécanisme de la REP apparaît comme un instrument privilégié pour inciter à la mise en place de modèles d'économie circulaire". [^14]

Les emballages à usage unique représente 86% des déchets soumis au mécanisme de la REP qui eux même représentent 10% des déchets collectés en Région bruxelloise. [^14]

[^14]: [Brussels environnement](https://environnement.brussels/lenvironnement-etat-des-lieux/en-detail/dechets/responsabilite-elargie-du-producteur)

Les entreprises peuvent décider de déléguer le REP de leurs emballages à Fost Plus et Valipac. "Elles paient alors un montant pour chaque emballage mis sur le marché, selon le tarif Point Vert par matériau. Ces tarifs prennent en considération les coûts de collecte, de tri et de recyclage mais pas seulement. Dans le calcul entrent en compte les recettes éventuellement générées par les matériaux recyclés ainsi que d’autres facteurs. Le Point Vert reflète ainsi le déficit dans la chaîne circulaire [...]. En outre, le principe de l’éco-modulation s’applique : plus les emballages sont difficilement recyclables, plus le montant sera élevé. Ceci afin d’encourager la mise sur le marché d’emballages facilement recyclables, compatibles avec les systèmes de collecte, de tri et de recyclage actuels." [^15]

[^15]: [Le Point Vert](https://www.fostplus.be/fr/blog/le-point-vert-pierre-angulaire-de-l-economie-circulaire)

![Point Vert](../images/project/Point_Vert.jpg)

Après avoir été triés et recyclés, les emballages peuvent retourner dans le circuit de consommation sous la forme d'un nouveau produit ou emballage grâce à Recyclis.

Voici quelques exemples permettant d'illustrer les avantages du recyclage du papier et du carton.

![](../images/project/recyclage_carton.JPG)

> "1 tonne de papier recyclé, soit l'équivalent de 200 000 feuilles de papier, permet :
 
> - la fabrication de 800kg de feuilles de papier, soit 320 bloc de feuilles d'impression
> - l'économie de 19 arbres, soit l'équivalent de 1,4 tonnes de bois
> - l'économie de 48m³ d'eau soit l'équivalent de 685 douches
> - l'économie de 10MWh, soit l'équivalent de la consommation d'un frigo A+ pendant 48 ans"

>"1 tonne de carton recyclé, soit l'équivalent de 5000 boites de chaussures, permet : 

> - la fabrication de 4000 boites de chaussures
> - l'économie de 19 arbres, soit l'équivalent de 1,4 tonnes de bois
> - l'économie de 1,4 tonnes de bois
> - l'économie de 
> - l'économie de 48m³ d'eau soit l'équivalent de 685 douches
> - l'économie de 10MWh, soit l'équivalent de la consommation d'un frigo A+ pendant 48 ans"

##### d. Comment valoriser le carton dans l'e-commerce ?

"Les efforts de durabilité sont principalement axés sur le packaging, mais pas seulement. [...] Que ce soit l’emballage en tant que tel, une manière plus efficace d’emballer ou le recyclage, il existe plusieurs axes envisagés. [...] Et donc, en ce qui concerne les emballages, les webshops s’appliquent de plus en plus à regrouper les commandes, limitent les matériaux de remplissage utilisés, réduisent le nombre de colis et utilisent progressivement des emballages réutilisables." [^16]

[^16]: [E-commerce et durabilité, un duo en progression](https://www.fostplus.be/fr/blog/e-commerce-et-durabilite-un-duo-en-progression)

Kazidomi, un e-commerce, fait justement attention à ses déchets et en parle dans une interview : "Tout d'abord, au niveau du packaging, on récupère les cartons des fournisseurs pour en faire des matériaux de protection. Nos clients finaux ont également la possibilité de nous renvoyer leurs cartons lors du prochain passage du livreur. Nous utilisons même du papier collant réalisé à partir de pelures de pommes de terre." (2021) [^17]

[^17]: [Interview de Emna Everard (Kazidomi)]https://lumiworld.luminus.be/fr/createurs-de-difference/createurs-de-difference-44-kazidomi/)

### 2.3 ***Act* - élabore une solution, mets la en oeuvre et obtiens un feedback**

#### A.  *Solution concepts*: quel est le sujet de votre solution ?

Je vais d'abord reprendre mes *guiding questions* :

* Est-ce que le carton peut réellement se recycler à l'infini ?
Comme nous avons pu le voir, le carton est un très bon candidat pour le recyclage. Il peut être recyclé de nombreuses fois et il peut également être valorisé entre les recyclages.

* Est-ce que les caisses en carton causent une partie de la déforestation ?
Il semblerait que non tant que nous nous procurons du carton ayant le label FSC ou bien issus de déchets de bois.

* Peut-on faire quelque chose d'utile des boîtes en cartons usagées dans un entrepôt ?
Kazidomi utilise du carton pour protéger ses bocaux en verre et également pour occuper du volume dans le colis afin de stabiliser les produits dans la caisse. D'autres entreprises du e-commerce utilise également du carton pour occuper du volume. Donc il semblerait qu'on puisse faire quelque chose d'utile à partir de carton.

* Peut-on rendre ces méthodes de valorisation plus accessibles ?
Si j'arrive à trouver un moyen low-tech pour valoriser le carton alors de petites entreprises pourraient se permettre de se procurer / de fabriquer cet outil et de valoriser leurs déchets en carton.
  
* Quelles sont les entreprises valorisant leurs caisses en carton ?
Comme dit précédemment, il y a en tout cas Kazidomi.

J'ai décidé de partir sur de la valorisation du carton en entrepôt et plus particulièrement sur la conception de protection des bocaux en verre à partir des caisses en carton usagées.

## 3. *17 sustainable development goals of the United Nations* [^18]

En ayant mes [*guiding questions*](#a-guiding-questions-quelles-sont-les-questions-auxquelles-il-faut-répondre-pour-relever-le-défi--quelle-est-la-priorité-) en tête, deux des 17 SDG me semblaient plus proches de ma problématique.

**Goal 12: Ensure sustainable consumption and production patterns**

![](../images/project/SDG_report_2021_Goal12.jpeg) [^18]

Les targets qui semblent connectées avec ma problématique :

Target 12.2 : By 2030, achieve the sustainable management and efficient use of natural resources

Indicator 12.2.1 : material footprint, material footprint per capita, and material footprint per GDP

Indicator 12.2.2 : domestic material consumption, domestic material comsumption per capita and domestic material consumption per GDP


Target 12.5 : By 2030, substantially reduce waste generation through prevention, reduction, recycling and reuse

Indicator 12.5.1 : national recycling rate, tons of material recycled


Target 12.8 : By 2030, ensure that people everywhere have the relevant information and awareness for sustainable development and lifestyles in harmony with nature

Indicator 12.8.1 : extent to which global citizenship education and education for sustainable development are mainstreamed in national education policies; curricula; teacher education; and student assessment

**Goal 15: Protect, restore and promote sustainable use of terrestrial ecosystems, sustainably manage forests, combat desertification, and halt and reverse land degradation and halt biodiversity lost**

![](../images/project/SDG_report_2021_Goal15.jpeg) [^18]

Les targets qui semblent connectées avec ma problématique :

Target 15.1 : By 2020, ensure the conservation, restoration and sustainable use of terrestrial and inland freshwater ecosystems and their services, in particular forests, wetlands, mountains and drylands, in line with obligations under international agreements

Indicator 15.1.1 : forest area as a proportion of total land area

Indicator 15.1.2 : proportion of important sites for terrestrial and freshwater biodiversity that are covered by protected areas, by ecosystem type

[^18]: [17 sdg of the United Nations](https://sdgs.un.org/goals)

Mais après avoir fait mes [recherches](#c-analysis-and-synthesis-rédiger-un-résumé-de-vos-conclusions-des-faits-et-des-données-receuillies), il me semble que l'objectif de développement durable le plus en lien avec ma problématique est le douzième : **"Établir des modes de consommation et de production durables"**.

En effet, nous pouvons retrouver dans le rapport 2021 sur les ODD concernant le *goal* 12 : "L'augmentation rapide de la consommation de ressources naturelles n'est pas viable. [...] Une voie vers une consommation et une production durables passe par l'économie circulaire, pensée pour réduire ou éliminer les déchets et la pollution, maintenir les produits et les matériaux en service, et régénérer les systèmes naturels". [^19]

[^19]: Rapport sur les objectifs de développement durable 2021 (Nations Unies)

## 4. Développement de la solution

### 4.1. Premier prototypage

Afin de protéger les bocaux et bouteilles en verre, j'ai pensé à une solution que l'on retrouve souvent : la gaine filet.

![](../images/project/Gaine_Filet_Mousse.jpg) [^20]

[^20]: [Image gaine filet](https://www.toutembal.fr/gaine-plastique-de-conditionnement__gaine-filet-mousse_44_CALAGAMO_c.html)

Et je me suis souvenue que lors de notre module sur la découpeuse laser, nous avions vu le [module de Nicolas De Coster](https://fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03) où il expliquait comment il rendait le [bois flexible](https://fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03#flexWoodGenIntro) grâce à des découpes, j'ai décidé de faire la même chose avec du carton ondulé. Je me suis aidée de son générateur de fichier SVG pour réaliser mes tests.

![](../images/project/generateur.JPG)

Dans un premier temps, j'ai fait les découpes avec un cutter sur un morceau de carton afin de voir s'il était possible de le rendre plus souple avec ce genre de pattern.

Une fois la découpe réalisée j'ai essayé d'étendre mon carton afin d'obtenir quelque chose ressemblant à une maille filet, mais il ne s'étendait pas. Par contre, si les découpes sont faites parallèlement aux ondulations, alors on peut remarquer que le carton est plus souple et permet de s'enrouler autour d'un objet. Le carton ne se plie que dans une seule direction mais pour protéger des bocaux ou des bouteilles en verre cela n'est pas un problème. Je suis donc partie sur l'idée de réaliser ces découpes afin d'avoir une protection en carton souple et non une gaine filet.

### 4.2. Paramétrage du pattern de découpe

Une fois que le pattern était confirmé, j'ai réalisé des tests grâce à la découpeuse laser Epilog en faisant varier certains paramètres du pattern.

Dans le générateur de Nicolas de Coster, il y a 7 paramètres différents :

- *Width*
- *Height*
- *Cut to border distance*
- *Top blank*
- *Bottom blank*
- *Space between lines*
- *Number of lines*

Pour chacun des paramètres j'ai décidé d'une valeur moyenne permettant d'avoir un pattern ressemblant à celui que j'avais découpé au cutter. Ensuite j'ai décidé de faire varier cette valeur en la diminuant et en l'augmentant pour les paramètres suivants : *cut to border distance*, *Space between lines* et *number of lines*. Il en a résulté 9 tests.

| Paramètres / Nom | distance1 | distance2 | distance3 | space1 | space2 | space3 | number1 | number2 | number3 |
| --: | --: | --: | --: | --: |--: | --: | --: | --: | --: |
| **Width** | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |
| **Height** | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |
| **Cut to border distance** | <span style="color:red">2.5</span> | <span style="color:red">5</span> | <span style="color:red">7.5</span> | 5 | 5 | 5 | 5 | 5 | 5 |
| **Top blank** | 5 | 5 | 5 | 5 | 5 | 5 | 5 | 5 | 5 |
| **Bottom blank** | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 |
| **Space between lines** | 5 | 5 | 5 | <span style="color:red">2.5</span> | <span style="color:red">5</span> | <span style="color:red">7.5</span> | 5 | 5 | 5 |
| **Number of lines** | 4 | 4 | 4 |4 | 4 | 4 |<span style="color:red">2</span> | <span style="color:red">4</span> | <span style="color:red">6</span> |

Pour faire les découpes j'ai utilisé du carton ondulé double face que j'ai recupéré de chez moi ainsi que la découpeuse laser Epilog. J'ai suivi [mon module sur la découpe assistée par ordinateur](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module03/) pour choisir les paramètres de l'Epilog et je me suis inspiré du travail de groupe puisque nous avions travaillé sur du papier cartonné. J'ai donc choisi :

- *speed* = 50
- *power* = 30

Seulement cela était trop rapide ça chauffait très fort le carton et ce n'était pas assez puissant pour couper à travers toute l'épaisseur du carton comme nous pouvons le voir sur la photo suivante.

![](../images/project/premier_essai.jpg)

J'ai décidé du coup de prendre comme paramètre :

- *speed* = 25
- *power* = 50

Et cela fonctionnait très bien.

![](../images/project/fourne.jpg)

Voici les résultats obtenus en variant le paramètre *cut to border distance* :

![](../images/project/distance.jpg)

Pour les tester, en ayant les découpes horizontales, j'attrapais le morceau de carton en tenant entre mes doigts les côtés supérieurs et inférieurs et je regardais si le carton se déformait ou bien s'il n'était pas assoupli et se soulevait de manière rigide. **Distance1** se plie légèrement, par contre **distance2** et **distance3** sont parfaitement rigide comme nous pouvons le voir sur les vidéos suivantes.

<video controls width="600" height="300">
 <source src="../images/project/distance1_video.mp4" type="video/mp4">
</video>

<video controls width="600" height="300">
<source src="../images/project/distance3_video.mp4" type="video/mp4">
</video>

Voici les résultats obtenus en variant le paramètre *space between lines* :

![](../images/project/space.jpg)

Les cartons **space2** et **space3** sont rigides lors du test, par contre **space1** est déjà bien souple.

<video controls width="600" height="300">
<source src="../images/project/space1_video.mp4" type="video/mp4">
</video>

Par contre lorsque je place les découpes verticalement et que j'attrape le carton de la même manière, cette fois-ci il reste rigide. Il est bien souple uniquement dans une seule direction.

<video controls width="600" height="300">
<source src="../images/project/space1_video_sens.mp4" type="video/mp4">
</video>

Et enfin voici les résultats obtenus en variant le paramètre *number of lines* :

![](../images/project/number.jpg)

À nouveau, **number2** et **number3** sont rigides, par contre **number1** est bien souple.

<video controls width="600" height="300">
<source src="../images/project/number1_video.mp4" type="video/mp4">
</video>

Ayant ces résultats en tête, j'ai décidé de garder les paramètres du **number1** et de **space1**, ainsi que de **distance1** même si les résultats sont moins fructueux, et de les faire varier avec les valeurs moyennes. J'obtiens donc 8 nouvelles combinaisons à tester.

| Paramètres / Nom | combi1 | combi2 | combi3 | combi4 | combi5 | combi6 | combi7 | combi8 |
| --: | --: | --: | --: | --: |--: | --: | --: | --: |
| **Width** | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |
| **Height** | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |
| **Cut to border distance** | <span style="color:#00FF00">2.5</span> | <span style="color:#00FF00">2.5</span>  | <span style="color:#00FF00">2.5</span>  | <span style="color:#00FF00">2.5</span>  | <span style="color:#FF6600">5</span>  | <span style="color:#FF6600">5</span> | <span style="color:#FF6600">5</span> | <span style="color:#FF6600">5</span> |
| **Top blank** | 5 | 5 | 5 | 5 | 5 | 5 | 5 | 5 |
| **Bottom blank** | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 | 2.5 |
| **Space between lines** | <span style="color:#00FF00">2.5</span> | <span style="color:#00FF00">2.5</span> | <span style="color:#FF6600">5</span> | <span style="color:#FF6600">5</span> | <span style="color:#00FF00">2.5</span> | <span style="color:#00FF00">2.5</span>| <span style="color:#FF6600">5</span> | <span style="color:#FF6600">5</span> |
| **Number of lines** | <span style="color:#00FF00">2</span> | <span style="color:#FF6600">4</span> | <span style="color:#00FF00">2</span> | <span style="color:#FF6600">4</span> | <span style="color:#00FF00">2</span> | <span style="color:#FF6600">4</span> |<span style="color:#00FF00">2</span> | <span style="color:#FF6600">4</span> |

Voici les résultats obtenus :

![](../images/project/combis.jpg)

Les cartons **combi4** et **combi8** sont toujours rigides, en effet ils n'ont pas que des paramètres avec des valeurs moyennes (sauf pour le *cut to border distance* mais il semblerait que celui-ci ne change pas grand chose vu les résultats de ces tests-ci). Les cartons **combi3** et **combi7** se plient légèrement. Les cartons **combi2** et **combi6** sont un petit peu souples, mais il n'est pas possible de les enrouler. Finalement, les cartons **combi1** et **combi5**, combinant tous les paramètres favoris, sont très souples.

<video controls width="600" height="300">
<source src="../images/project/combi1_video.mp4" type="video/mp4">
</video>

<video controls width="600" height="300">
<source src="../images/project/combi5_video.mp4" type="video/mp4">
</video>

Par contre, **combi1** est si souple, qu'il était possible de l'étirer. Mais de cette manière là la protection est bien moins épaisse et moins dense et du coup moins solide. Je reste donc sur ma décision de faire du carton souple et plus une gaine filet.

![](../images/project/combi1_étendu.jpg)

Pour choisir entre **combi1** et **combi5**, j'ai décidé de partir sur le pattern le plus facile à réaliser. J'ai choisi **combi5** car *cut to border distance* est plus élévé et donc il y a un moins grand risque que le carton se déchire lors de la découpe et de l'utilisation. Les paramètres finaux du pattern sont donc :

- **Width** = 100
- **Height** = 100
- **Cut to border distance** = 5
- **Top blank** = 5
- **Bottom blank** = 2.5
- **Space between lines** = 2.5
- **Number of lines** = 2

### 4.3. Comment réaliser cette découpe ?

Evidemment je ne m'attends pas à ce que chaque entrepôt se procure une découpe laser très couteuse pour valoriser leur déchets en carton. Je vais donc maintenant m'intéresser à comment réaliser cette découpe de manière low-tech.

Lors d'un brainstorming en cours, en expliquant ma problématique, nous avons eu plusieurs idées : la première étant de réaliser une presse composée de plaques, l'une d'entre elles comporte des lames représentant le pattern de découpe et l'autre comporte le même pattern mais cette fois-ci avec des renfoncements permettant aux lames de s'enfoncer; la seconde solution consistant en deux rouleaux entre lesquels nous passerions le morceau de carton et il en sortirait découpé.

En faisant des recherches sur internet afin de trouver des idées de conception, je n'ai rien trouvé. Par contre, j'ai commencé à immaginé un rouleau, comme un rouleau de patisserie, avec des lames, et en recherchant "rouleau à découpe" je suis tombée sur ceci :

![](../images/project/croisillons.jpg)[^21]

[^21]:[Image de rouleau à losange](https://www.lessecretsduchef.be/fr/decoration-et-design/7287-westmark-rouleau-a-croisillons-9900000012311.html)

Il s'agit d'un rouleau à losange permettant de faire la partie décorative des tartes en découpant dans de la pâte feuilletée. Et il s'agit du même pattern que j'utilise ! J'ai donc décidé de m'inspirer de cet outil pour fabriquer le mien.

### 4.4. Comment fabriquer le rouleau ?

La première étape a été de décider quel matériau prendre pour fabriquer la lame. J'ai essayé de couper du carton ondulé avec mon rouleau à losange de patisserie mais cela n'a rien coupé du tout. Je me suis donc dit que de l'imprimer en 3D avec du PLA ne fonctionnerait pas. J'ai donc décidé de partir sur du métal. 
J'ai commencé à concevoir mes pièces en 3D sur OpenSCAD en paramétrant le tout; il est possible de choisir la longueur de coupe souhaitée, la longueur entre les coupes ainsi que le nombre de "dents" nous voulons sur notre lame. Voici à quoi l'esquisse ressemble pour 2 et 5 dents :

![](../images/project/dents.jpg)

Ensuite, avant de décider quel feuille de métal j'allais acheter et comment j'allais réussir à les aiguiser, j'ai regardé s'il existait des lames circulaires. Il se trouve qu'il existe des cutters circulaires et qu'il est possible d'acheter leurs lames séparemment. J'ai donc décidé de partir sur ce genre de lames.

![](../images/project/lame_look.jpg)

En mesurant sur le morceau de carton **combi5**, j'ai défini que la longueur de coupe nécessaire est de 57mm et l'espace vertical entre les coupes est de 10mm. Ce qui nous donne un diamètre de 42,65mm si nous réalisons 2 dents sur la lame ou de 63.98mm si nous en réalisons 3. Les lames de cutters disponibles sont de diamètre 45mm ou bien 65mm ce qui est très proche de nos mesures. Pour une facilité d'utilisation j'ai décidé de prendre celles de 45mm de diamètres.

Comme le diamètre a changé, il faut que j'ajuste les longueurs de coupe et d'espace. En effet, je ne veux pas retirer de la matière sur les lames pour diminuer leur diamètre puisque cela retirerait l'aiguisage. Du coup, la question que je me pose maintenant est que vaut-il mieux faire : allonger de quelques mm la coupe ou l'espace ? Pour cela, j'ai regardé les cartons **distance** et **number**. **Distance1** était légèrement mieux que les autres **distance** donc allonger l'espace a un leger impact. **Number1** était bien mieux que les autres **number** donc allonger la coupe a un fort impact. J'ai donc décidé de prendre ces nouvelles longueurs : 

- coupe = 60,7 mm
- espace = 10 mm

Pour découper les petites sections dans les lames afin de respecter les espacements entre les coupes, je me suis demandé si j'essayais d'utiliser la CNC ou bien si je le faisais avec des outils plus petits comme une cisaille à métaux. Puisque ma problématique est de rendre la solution de valorisation du carton plus accessible, j'ai décidé de ne pas utiliser la CNC, n'ayant moi-même pas fait la formation pour celle-ci. 

![](../images/project/cisaille.jpg)

Malheureusement même s'il s'agit d'une cisaille à métaux, dont l'acier inoxidable en quoi sont faites les lames, la lame s'est cassé au moment de la coupe. Après plusieurs essais, la lame se casse à chaque fois.

![](../images/project/lame_casse.jpg)

J'ai donc décidé d'utiliser un dremel. J'ai d'abord réalisé un prototype en plastique de ma lame afin de pouvoir l'utiliser pour marquer les sections à retirer. Ensuite, en ayant les protections suffisantes (masque, lunettes de protections et gants), j'ai appris à utiliser le dremel. Avec le matériel que j'avais à disposition, j'ai fixé la lame en l'intercallant entre des pièces en caoutchouc afin d'amortir les vibrations pour éviter que la lame se casse. Voici les différentes pièces et outils utilisés :

![](../images/project/fixation.jpg)

<video muted controls width="600" height="300">
<source src="../images/project/dremel_video.mp4" type="video/mp4">
</video>

Voici le premier résultat lorsque je prenais la machine en main : 

![](../images/project/lame1.jpg)

Ensuite, voici le résultat des autres lames :

![](../images/project/lames.jpg)

J'ai pu en faire sept (puisque les trois autres m'ont servi de test pour la cisaille et le dremel). J'ai dû faire attention à l'orientation des lames avant de les couper puisqu'il qu'il faut avoir un décalage d'un quart de tour entre deux lames pour respecter le pattern que j'ai défini plus tôt.

Maintenant que les lames sont prêtes, il faut qu'il je dessine le manche. Pour comprendre le fonctionnement du rouleau à losange de pattiserie, je l'ai demonté. J'ai décidé de réaliser le même fonctionnement mais avec des formes plus simples pour faciliter le dessin et l'impression.

J'ai commencé par l'axe central sur lequel seront mis les lames. Pour cela j'ai reproduit la forme que nous pouvons retrouvé au centre des lames de cutter.

![](../images/project/axe_openscad.JPG)

Pour les dimensions, j'ai tracé la forme depuis la lame sur une feuille en papier et puis j'ai mesuré. J'ai decidé d'imprimer qu'un axe très court au début uniquement pour voir si les dimensions étaient bonnes. Le premier essai n'était pas le bon, les dimensions étaient trop petites. Par contre le deuxième était bon et rentrait parfaitement au centre de la lame.

![](../images/project/axes.jpg)

Ensuite, pour que les coupes soient espacées horizontalement de 2mm (comme nous l'avons défini plus tôt dans les tests), j'ai dessiné des disques à intercaler sur l'axe entre deux lames.

![](../images/project/disques.JPG)

J'en ai imprimé un premier pour m'assurer que la forme centrale était compatible avec les dimensions de l'axe, ce qui était le cas. Ensuite j'en ai imprimé sept puisqu'il m'en faut une entre chaque lame et également sur les bords ce qui en fait huit (et j'en avais déjà imprimée une).

![](../images/project/disques_impression_axe.jpg)

Finalement il reste le manche. Pour cela je me suis basée sur la taille d'une fluo stabilo pour les dimensions du manche qu'on devra prendre en main et sur le dessus j'ai ajouté un support pour accrocher l'axe. 

![](../images/project/manche_openscad.JPG)

Une fois le manche imprimé, je me suis rendue compte qu'il allait être difficile d'écarter suffisamment les supports du manche pour pouvoir glisser l'axe central (surtout qu'il faut être prudent en manipulant les sept lames de cutter). J'ai donc coupé au cutter les supports de moitié. Seulement cela ne suffisait pas, par conséquent j'ai encore coupé un des deux supports.

![](../images/project/manches.jpg)

Grâce à cet ajustement, j'ai pu insérer l'axe sur le manche et ajouter les lames ainsi que les disques. J'ai mis à chaque fois une lame - en faisant attention à l'orientation de celle-ci - puis un disque, etc (j'ai par contre oublié de commencer par un disque et il n'était pas possible de le rajouter par la suite sans tout recommencer). Voici à quoi notre découpeuse ressemble :

![](../images/project/ensemble1.jpg)

Malheureusement, il est très difficile de découper du carton avec. Les lames ne s'enfoncent pas et les supports s'écartent. J'ai donc essayé de mettre uniquement quatre lames et plus sept et d'insérer deux disques et plus un entre les lames. Voici à quoi la découpeuse ressemble ainsi :

![](../images/project/ensemble2.jpg)

Avec ce modèle là, j'ai pu découper un morceau de carton ondulé. Il a fallu repasser plusieurs fois et de chaque côté mais cela donne rapidement quelque chose de pas mal et de souple.

![](../images/project/resultat.jpg)

## 5. Résumé de la fabrication du prototype final

|:--|:--|:--|
|Matériel | Prix | Commentaires |
|Lames de cutter circulaire | soit les [officielles](https://www.cutter-olfa.fr/contents/fr/p93_Lames-RB45-cutter-rotatif-45-mm.html) à 49,99€, soit les [imitations](https://www.amazon.fr/gp/product/B01DALASFC/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) à 11,99€ (celles que j'ai prises) | |
|Dremel et accessoires | | Emprunté |
| Matériel de protection | | Emprunté |
| PLA (24,99€/kg) | 1,05€ | 1,17g pour l'axe, 18,14g pour les disques, 22,52g pour le manche |
| Impression 3D (1,5€/h) | 5,25€ | 16min pour l'axe, 1h17 pour les disques, 1h53 pour le manche |
| **Total** | 18,29€ | |

- 10 lames de cutter circulaire : soit les [officielles](https://www.cutter-olfa.fr/contents/fr/p93_Lames-RB45-cutter-rotatif-45-mm.html) à 49,99€, soit les [imitations](https://www.amazon.fr/gp/product/B01DALASFC/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) à 11,99€ (celles que j'ai prises)
- un dremel et accessoires
- matériel de protection
- une bobine de PLA
- une imprimante 3D (au Fablab il s'agit d'imprimantes Prusa)

Les différents fichiers se trouvent sur gitlab :

- axe central : *axe.openSCAD*, *axe.stl* (pour PrusaSlicer), *axe.gcode* (pour l'imprimante - 16min) ainsi que *axe2.stl* et *axe2.gcode* pour tester les dimensions
- disques pour espacer les lames : *space.openSCAD*, *spaces.stl* et *spaces.gcode* (1h07 pour sept disques)
- manche : manche.openSCAD, manche.stl et manche.gcode (1h53)

Il faut compter environ 3h30 d'impression.


Tous les fichiers sont paramétriques, il est donc très facile de modifier les dimensions.

Sur gitlab, il y a également le fichier *disque_decoupe.openscad* si vous désirez découper les lames.

## 6. Pistes d'amélioration

Si j'avais plus de temps pour continuer ce projet voici les pistes sur lesquelles je travaillerais, ou bien si vous voulez vous plongez sur le sujet voici des pistes d'amélioration : 

- faire varier le diamètre des disques entre les lames pour permettre aux lames de s'enfoncer plus ou moins dans le carton 
- faire varier la profondeur de la section d'entaille sur lame
- faire varier le nombre de lames présentent sur la découpeuse
- faire varier le nombre de disques entre les lames
- trouver une solution pour assouplir les supports et faciliter l'insertion de l'axe sur le manche
- faire varier la longueur des supports circulaires pour l'axe afin de trouver un équilibre entre la possibilité d'assembler les pièces facilement et celle que les supports ne s'écartent pas pendant l'utilisation de l'outil
- essayer de réaliser les lames à partir d'une feuille de métal à la CNC et voir si cela : fonctionne mieux, prend moins de temps, etc...

## 7. Conclusion

Au final, j'ai réussi a à définir une problématique bien précise en partant d'un questionnement assez général. J'ai réussi à réaliser un prototype qui facilite la découpe de carton ondulé (en comparaison à le faire uniquement avec un simple cutter à la main). Il y a donc moyen de réaliser ce genre d'outil. 
Par contre, je ne pense pas qu'il soit déjà suffisamment adéquat pour qu'un entrepôt prenne le temps et l'argent de le fabriquer (même s'il s'agit de 5h maximum avec tout le matériel à disposition et de 19€). Mais il y a plein de pistes à explorer pour l'améliorer.


