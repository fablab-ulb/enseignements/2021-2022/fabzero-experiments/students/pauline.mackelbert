Bienvenue sur ma page web FabLab !

## Qui suis-je ?

Bonjour ! Je m'appelle Pauline Mackelbert.

![](images/pauline.jpg)

 Je suis étudiante à l'École polytechnique de Bruxelles à l'ULB. Je suis en Master 2 en section informatique.

Je suis cette année le cours "How To Make (almost) Any Experiments" et sur ce site web vous pourrez suivre mon travail !

J'aime me balader dans la nature (spécialement à la montagne), cuisiner et bien sûr manger de bonnes choses, écouter de la musique, lire un bon livre, regarder la même série en boucle, chanter et faire des claquettes.

Le monde évolue dans tous les sens du terme et je trouvais cela fondamental d'avoir un aspect durable du rôle de l'ingénieur. En dehors des études je lis beaucoup de livres sur les lowtechs, sur de nouveaux systèmes sociétaux, et sur plein d'autres sujets. Mais je n'avais pas encore eu de cours parlant spécifiquement de l'avenir. Lorsque j'ai vu dans la liste des cours un cours voulant nous pousser à réfléchir à des problèmes de société touchant les 17 objectifs du développement durable [^1] et à développer une solution j'ai tout de suite été intéressée. Ensuite, j'ai découvert que nous allions apprendre à utiliser les machines de fabrication numériques présentes au FabLab ULB et à partir de ce moment là je savais que je choisirais ce cours.

[^1]: [sustainable development goals](https://sdgs.un.org/goals)

## Mon background

Je suis donc étudiante pour devenir ingénieure civile informatique. Mais j'avais d'abord commencé un master en électromécanique en spécialité Mécatronique, suivi par le master en électromécanique spécialité gestion. Mais aucun des deux ne me convenaient et je suis passée en spécialité informatique. Malgré que je n'ai pas fini ces masters, j'ai pu, pendant ces deux années, en apprendre énormément sur le sujet en réalisant des projets dans les différentes matières (robotique, supply chain management, electrical drives, etc).

L'avantage de mes études est qu'en master nous avons beaucoup de cours à la carte. J'ai pu donc me faire un programme correspondant à mes envies et besoin futurs. J'ai réalisé un stage dans une start up [Eat's Local](https://www.bwbx.eatslocal.be/) où j'ai travaillé en tant que consultante pour les aider à optimiser leur logistique et gestion d'entrepôt en mettant en place un outil informatique. J'ai aussi eu l'opportunité d'être chef de projet en gérant un groupe de 5 étudiants de BA1 et en les accompagnant tout au long de leur projet d'année. J'ai suivi des cours classiques en informatique comme *Database systems architecture*, *Introduction to cryptography*, *Introduction to language theory and compiling*, *Communication networks*, *Microprocessor architecture*, *Image acquisition and processing*, ... Mais j'ai également eu des cours plus différents comme *Business process management* et celui-ci *How to make (almost) any experiment using digital fabrication*.